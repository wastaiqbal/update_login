<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Data Alumni Unas</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url("vendor/fontawesome-free/css/all.min.css ")?>" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="<?php echo base_url("vendor/datatables/dataTables.bootstrap4.css ")?>" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href=" <?php echo base_url("css/sb-admin.css")?>" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="index.html">Start Bootstrap</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="<?php echo base_url("#")?>">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
      <div class="input-group">
        <div class="input-group-append">
        <div style="margin-top: 50px;" color="white"><center>
        
        
        
        </center></div>
        </div>
      </div>
    </form>

     <!-- Navbar -->
     <ul class="navbar-nav ml-auto ml-md-0">
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
         
        </a>
      
      </li>
      <li class="nav-item dropdown no-arrow mx-1">
        
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
          <a class="dropdown-item" href="<?php echo base_url("")?>"><?php echo $this->session->userdata('nama'); ?></a>
         
        </div>
      </li>
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="<?php echo base_url("")?>" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-circle fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          <a class="dropdown-item" href="<?php echo base_url('index.php/Layout/edit_akun?ni=') . $this->session->userdata('npm'); ?>"><?php echo $this->session->userdata('nama'); ?></a>
          
          <a class="dropdown-item" href="" data-toggle="modal" data-target="#logoutModal">Logout</a>
        </div>
      </li>
    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url("index.php/Layout/menu")?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
      
      
      <li class="nav-item">
        <a class="nav-link" href=" <?php echo base_url('index.php/Layout/view')  ?>">
          <i class="fas fa-fw fa-table"></i>
          <span>Tables</span> </a>
      </li>
    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a>
            <?php
            $alumni = $this->session->userdata('lab');
            echo $alumni;
            
          ?>
            </a>
          </li>
          
        </ol>

        <!-- Icon Cards-->
        <div class="row">
        
        </div>

        <!-- Area Chart Example-->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-chart-area"></i>
            Edit Mata Kuliah</div>
          <div class="card-body">
            
		  <form action="<?php echo base_url('index.php/Matkul/ubah_matkul') ?>"  method="post" enctype="multipart/form-data">
<?php

$nim = $this->input->get('ni');
$lab = $this->session->userdata('lab');
$cek=$this->UserMatkul->get_edit($nim, $lab);

foreach ($cek->result_array() as $row)
{  
    
    
?>
    
    <div class="container">
		
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> ID MATA KULIAH </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="kode_matkul" value=<?php echo $row['kode_matkul'] ?> >
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> MATA KULIAH </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="matkul" value="<?php echo $row['matkul'] ?>">
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> LABORATORIUM </label>
			<div class="col-sm-8">
                <select class="form-control" name="lab" required >  
                    <option <?php echo ($row['Laboratorium'] == 'E-Commerce') ? "selected": "" ?> > E-Commerce </option>
                    <option <?php echo ($row['Laboratorium'] == 'Multimedia and Computer Vision') ? "selected": "" ?> > Multimedia and Computer Vision </option>
                    <option <?php echo ($row['Laboratorium'] == 'Network Data and Communication') ? "selected": "" ?> > Network Data and Communication </option>
					<option <?php echo ($row['Laboratorium'] == 'Artificial Intelegent') ? "selected": "" ?> > Artificial Intelegent </option>
                </select>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> HARI </label>
			<div class="col-sm-8">
                <input type="radio" name="hari" value="SENIN" <?php echo ($row['hari'] == 'SENIN') ? "checked": "" ?> > SENIN
                <input type="radio" name="hari" value="SELASA" <?php echo ($row['hari'] == 'SELASA') ? "checked": "" ?> > SELASA 
                <input type="radio" name="hari" value="RABU" <?php echo ($row['hari'] == 'RABU') ? "checked": "" ?> > RABU <br>
                <input type="radio" name="hari" value="KAMIS" <?php echo ($row['hari'] == 'KAMIS') ? "checked": "" ?> > KAMIS 
                <input type="radio" name="hari" value="JUMAT" <?php echo ($row['hari'] == 'JUMAT') ? "checked": "" ?> > JUMAT
                <input type="radio" name="hari" value="SABTU" <?php echo ($row['hari'] == 'SABTU') ? "checked": "" ?> > SABTU 
			</div>
		</div>
        <div class="form-group row">
                <label class="col-sm-2 col-form-label"> JAM </label>
                <div class="col-sm-8">	
                <select name="jam" class="form-control" required>
                    <option selected disabled>....</option>
					<option <?php echo ($row['jam'] == '08.00-09.40') ? "selected": "" ?> > 08.00-09.40 </option>
					<option <?php echo ($row['jam'] == '09.50-11.30') ? "selected": "" ?>> 09.50-11.30 </option>
					<option <?php echo ($row['jam'] == '11.40-13.20') ? "selected": "" ?>> 11.40-13.20 </option>
					<option <?php echo ($row['jam'] == '13.30-15.10') ? "selected": "" ?>> 13.30-15.10 </option>
					<option <?php echo ($row['jam'] == '15.20-17.00') ? "selected": "" ?>> 15.20-17.00 </option>
					<option <?php echo ($row['jam'] == '17.00-18.40') ? "selected": "" ?>> 17.00-18.40 </option>
					<option <?php echo ($row['jam'] == '18.50-20.30') ? "selected": "" ?>> 18.50-20.30 </option>
					<option <?php echo ($row['jam'] == '20.40-22.00') ? "selected": "" ?>> 20.40-22.00 </option>
			    </select>
                </div>
        </div>	
        <div class="form-group row">
                <label class="col-sm-2 col-form-label"> KELAS </label>
                <div class="col-sm-8">	
                <select name="kelas" class="form-control" required>
                    <option selected disabled>....</option>
					<option <?php echo ($row['kelas'] == 'R01') ? "selected": "" ?>> R01 </option>
					<option <?php echo ($row['kelas'] == 'R02') ? "selected": "" ?>> R02 </option>
					<option <?php echo ($row['kelas'] == 'R03') ? "selected": "" ?>> R03 </option>
					<option <?php echo ($row['kelas'] == 'R04') ? "selected": "" ?>> R04 </option>
					<option <?php echo ($row['kelas'] == 'R05') ? "selected": "" ?>> R05 </option>
					<option <?php echo ($row['kelas'] == 'R06') ? "selected": "" ?>> R06 </option>
					<option <?php echo ($row['kelas'] == 'R07') ? "selected": "" ?>> R07 </option>
					<option <?php echo ($row['kelas'] == 'R08') ? "selected": "" ?>> R08 </option>
					<option <?php echo ($row['kelas'] == 'R09') ? "selected": "" ?>> R09 </option>
					<option <?php echo ($row['kelas'] == 'R10') ? "selected": "" ?>> R10 </option>
					<option <?php echo ($row['kelas'] == 'R11') ? "selected": "" ?>> R11 </option>
					<option <?php echo ($row['kelas'] == 'R12') ? "selected": "" ?>> R12 </option>
                    <option <?php echo ($row['kelas'] == 'K01') ? "selected": "" ?>> K01 </option>
					<option <?php echo ($row['kelas'] == 'K02') ? "selected": "" ?>> K02 </option>
					<option <?php echo ($row['kelas'] == 'K03') ? "selected": "" ?>> K03 </option>
					<option <?php echo ($row['kelas'] == 'K04') ? "selected": "" ?>> K04 </option>
					<option <?php echo ($row['kelas'] == 'K05') ? "selected": "" ?>> K05 </option>
					<option <?php echo ($row['kelas'] == 'K06') ? "selected": "" ?>> K06 </option>
					<option <?php echo ($row['kelas'] == 'K07') ? "selected": "" ?>> K07 </option>
					<option <?php echo ($row['kelas'] == 'K08') ? "selected": "" ?>> K08 </option>
			    </select>
                </div>
        </div>	
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> DOSEN </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="dosen" value="<?php echo $row['dosen'] ?>"  required>
			</div>
		</div>
		<div class="form-group row">
                <label class="col-sm-2 col-form-label"> TAHUN AJARAN </label>
                <div class="col-sm-8">	
                <select name="tahun_ajaran" class="form-control" required>
                    <option selected disabled>....</option>
					<option <?php echo ($row['tahun_ajaran'] == '2019-2020') ? "selected": "" ?>> 2019-2020 </option>
					<option <?php echo ($row['tahun_ajaran'] == '2020-2021') ? "selected": "" ?>> 2020-2021 </option>
					<option <?php echo ($row['tahun_ajaran'] == '2021-2022') ? "selected": "" ?>> 2021-2022 </option>
					<option <?php echo ($row['tahun_ajaran'] == '2022-2023') ? "selected": "" ?>> 2022-2023 </option>
					<option <?php echo ($row['tahun_ajaran'] == '2023-2024') ? "selected": "" ?>> 2023-2024 </option>
					<option <?php echo ($row['tahun_ajaran'] == '2024-2025') ? "selected": "" ?>> 2024-2025 </option>
                    <option <?php echo ($row['tahun_ajaran'] == '2025-2026') ? "selected": "" ?>> 2025-2026 </option>
					<option <?php echo ($row['tahun_ajaran'] == '2026-2027') ? "selected": "" ?>> 2026-2027 </option>
					<option <?php echo ($row['tahun_ajaran'] == '2027-2028') ? "selected": "" ?>> 2027-2028 </option>
                    <option <?php echo ($row['tahun_ajaran'] == '2027-2028') ? "selected": "" ?>> 2028-2029 </option>
                    <option <?php echo ($row['tahun_ajaran'] == '2027-2028') ? "selected": "" ?>> 2028-2029 </option>
			    </select>
                </div>
        </div>	
        
		<div class="form-group row">
		<div class="col-sm-10" style="float: right;">	
				<button class="btn btn-lg btn-danger" name="batal">BATAL</button>
				<button class="btn btn-lg btn-primary" name="tambah">TAMBAH</button>
		</div>	
		</div>
	</div>

<?php } ?>
</form>


          </div>
        </div>

          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

      

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Data Tracker Alumni Create by Danang 173112706450221</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href=" <?php echo base_url("#page-top")?>">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href=" <?php echo base_url('index.php/Layout/halaman_utama')  ?>">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url("vendor/jquery/jquery.min.js ")?>"></script>
  <script src="<?php echo base_url("vendor/bootstrap/js/bootstrap.bundle.min.js ")?>"></script>

  <!-- Core plugin JavaScript-->
  <script src=" <?php echo base_url("vendor/jquery-easing/jquery.easing.min.js")?>"></script>

  <!-- Page level plugin JavaScript-->
  <script src=" <?php echo base_url("vendor/chart.js/Chart.min.js")?>"></script>
  <script src="<?php echo base_url("vendor/datatables/jquery.dataTables.js ")?>"></script>
  <script src=" <?php echo base_url("vendor/datatables/dataTables.bootstrap4.js")?>"></script>

  <!-- Custom scripts for all pages-->
  <script src=" <?php echo base_url("js/sb-admin.min.js")?>"></script>

  <!-- Demo scripts for this page-->
  <script src="<?php echo base_url("js/demo/datatables-demo.js ")?>"></script>
  <script src=" <?php echo base_url("js/demo/chart-area-demo.js")?>"></script>

</body>

</html>