
<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Admin Area </title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- styles -->
  <link href="" href="<?php echo base_url("assets/css/bootstrap.css")?>" rel="stylesheet">
  <link href="" href="<?php echo base_url("assets/css/bootstrap-responsive.css")?>" rel="stylesheet">
  <link href="" href="<?php echo base_url("assets/css/docs.css")?>" rel="stylesheet">
  <link href="" href="<?php echo base_url("assets/css/prettyPhoto.css")?>" rel="stylesheet">
  <link href="" href="<?php echo base_url("assets/js/google-code-prettify/prettify.css")?>" rel="stylesheet">
  <link href="" href="<?php echo base_url("assets/css/camera.css")?>" rel="stylesheet">
  <link href="" href="<?php echo base_url("https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300|Open+Sans:400,300,300italic,400italic")?>" rel="stylesheet">
  <link href="" href="<?php echo base_url("assets/css/style.css")?>" rel="stylesheet">
  <link href="" href="<?php echo base_url("assets/color/success.css")?>" rel="stylesheet">

  <!-- fav and touch icons -->
  <link rel="shortcut icon" href="assets/ico/favicon.ico">
  <link rel="apple-touch-icon-precomposed" sizes="144x144"  href="<?php echo base_url("assets/ico/apple-touch-icon-144-precomposed.png")?>" >
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo base_url("assets/ico/apple-touch-icon-114-precomposed.png")?>" >
  <link rel="apple-touch-icon-precomposed" sizes="72x72"  href="<?php echo base_url("assets/ico/apple-touch-icon-72-precomposed.png")?>" >
  <link rel="apple-touch-icon-precomposed"  href="<?php echo base_url("assets/ico/apple-touch-icon-57-precomposed.png")?>" >

  <!-- =======================================================
    Theme Name: Scaffold
    Theme URL: https://bootstrapmade.com/scaffold-bootstrap-metro-style-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>

  <header>
    <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <!-- logo -->
          <a class="brand logo"  href="<?php echo base_url("index.html")?>" >
            <p> SELAMAT DATANG DIHALAMAN ADMIN <p>
          </a>
          <!-- end logo -->

          
        </div>
      </div>
    </div>
  </header>


  <!-- end tagline -->
	<br><br>
  <section id="maincontent">
    <div class="container">

      <div class="row">
        <div class="span3">
      
		  <h3 class="heading-success"><span class="btn btn-large btn-success"><i class="m-icon-big-swapright m-icon-white"></i></span>&nbsp;&nbsp;<font size="3"><?php echo $_SESSION['nama']; ?></font></h3>
		  <h3 class="heading-success"><span class="btn btn-large btn-success"><i class="m-icon-big-swapright m-icon-white"></i></span>&nbsp;&nbsp;<font size="3">Logout</h3>

        </div>
        <div class="span3">
          <div class="well well-primary box">
            <img src="<?php echo base_url("assets/img/icons/box-1-white.png ")?>" alt="" />
            <h3>SAINTEK</h3>
            <p>
				SILAHKAN KLIK UNTUK DATA SOAL SAINTEK
			  </p>
          <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
            <a href=" <?php echo base_url("soal/saintek/tkpa/index.php")?>" class="btn btn-dark"><font color = "black"><b>TKPA</b></font></a>
            <a href=" <?php echo base_url("soal/saintek/tkd_saintek/index.php")?> "  class="btn btn-dark"><font color = "black"><b>TKDA SAINTEK</b></font></a>
          </div>    
          </div>
        </div>
        <div class="span3">
          <div class="well well-success box">
            <img src=" <?php echo base_url("assets/img/icons/box-2-white.png")?>" alt="" />
            <h3>SOSHUM</h3>
            <p>
              SILAHKAN KLIK UNTUK DATA SOAL SOSHUM
            </p>
            <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
              <a href="<?php echo base_url("soal/soshum/tkpa/index.php ")?>" class="btn btn-dark"><font color = "black"><b>TKPA</b></font></a>
              <a href=" <?php echo base_url("soal/soshum/tkd_soshum/index.php")?>" class="btn btn-dark"><font color = "black"><b>TKDA SOSHUM</b></font></a>
            </div>   
          </div>
        </div>
        <div class="span3">
          <div class="well well-warning box">
            <img src=" <?php echo base_url("assets/img/icons/box-3-white.png")?>" alt="" />
            <h3>PENGGUNA</h3>
            <p>
              SILAHKAN KLIK UNTUK DATA PESERTA DAN ADMIN
            </p>
              <a href=" <?php echo base_url("user/admin/1/index.php")?>" class="btn btn-dark"><font color = "black"><b>ADMIN</b></font></a>
              <a href="<?php echo base_url("user/peserta/1/index.php ")?>" class="btn btn-dark"><font color = "black"><b>PESERTA</b></font></a>
          </div>
        </div>
      </div>

    </div>
  </section>


  <section id="bottom">
    <div class="container">

      <!-- divider -->
      <div class="row">
        <div class="span12">
          <div class="divider"> <h4><?php  date_default_timezone_set('Asia/Jakarta'); echo  date("l, j F Y, H:i");  ?> &nbsp; &nbsp; &nbsp; &nbsp;</h4></div>
        </div>
      </div>
      <!-- end divider -->


  <!-- Footer
 ================================================== -->
  <footer class="footer">
 
  </footer>

  <script src=" <?php echo base_url("assets/js/jquery-1.8.2.min.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/jquery.easing.1.3.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/google-code-prettify/prettify.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/modernizr.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/bootstrap.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/jquery.elastislide.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/jquery.flexslider.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/jquery.prettyPhoto.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/application.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/hover/jquery-hover-effect.js")?>"></script>
  <script src="<?php echo base_url("assets/js/hover/setting.js ")?>"></script>
  <script src="<?php echo base_url("assets/js/camera/camera.min.js ")?>"></script>
  <script src=" <?php echo base_url("assets/js/camera/setting.js")?>"></script>

  <!-- Template Custom JavaScript File -->
  <script src=" <?php echo base_url("assets/js/custom.js")?>"></script>

</body>

</html>
