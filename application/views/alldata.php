<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Data Alumni Unas</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url("vendor/fontawesome-free/css/all.min.css ")?>" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="<?php echo base_url("vendor/datatables/dataTables.bootstrap4.css ")?>" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href=" <?php echo base_url("css/sb-admin.css")?>" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="index.html">Start Bootstrap</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="<?php echo base_url("#")?>">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
        <div class="input-group-append">
          <button class="btn btn-primary" type="button">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-bell fa-fw"></i>
          <span class="badge badge-danger">9+</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
          <a class="dropdown-item" href="<?php echo base_url("")?>">Action</a>
          <a class="dropdown-item" href="<?php echo base_url("")?>">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url("")?>">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="<?php echo base_url("")?>" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-envelope fa-fw"></i>
          <span class="badge badge-danger">7</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
          <a class="dropdown-item" href="<?php echo base_url("")?>"><?php echo $this->session->userdata('nama_dosen'); ?></a>
          <a class="dropdown-item" href="<?php echo base_url("")?>">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url("")?>">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="<?php echo base_url("")?>" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-circle fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          <a class="dropdown-item" href="<?php echo base_url("")?>"><?php echo $this->session->userdata('nama_dosen'); ?></a>
          <a class="dropdown-item" href="<?php echo base_url("")?>">Activity Log</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url("")?>" data-toggle="modal" data-target="#logoutModal">Logout</a>
        </div>
      </li>
    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url("index.php/Layout/menu")?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
      
      
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url("index.php/Layout/view")?>"">
          <i class="fas fa-fw fa-table"></i>
          <span>Tables</span></a>
      </li>
    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a href="<?php echo base_url("index.php/Layout/track_study")?>"">Track study</a>
          </li>
          <li class="breadcrumb-item active">
            <a href="<?php echo base_url("index.php/Layout/stcakholder")?>"">Stakeholder</a>
          </li>
        </ol>

        <!-- Icon Cards-->
        <div class="row">
        
        </div>

      

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Berikut merupakan data keseluruhan Stcakholder
          <?php
            $alumni = $this->session->userdata('prodi');
            
            if($alumni == 1){
                echo "Ilmu Politik / Fakultas Ilmu Sosial dan Politik";
            }
            else if($alumni == 2){
                echo "Administrasi Negara / Fakultas Ilmu Sosial dan Politik";
            }
            else if($alumni == 3){
                echo "Hubungan Internasional / Fakultas Ilmu Sosial dan Politik";
            }
            else if($alumni == 4){
                echo "Sosiologi / Fakultas Ilmu Sosial dan Politik";
            }
            else if($alumni == 5){
                echo "Ilmu Komunikasi / Fakultas Ilmu Sosial dan Politik";
            }
            else if($alumni == 6){
                echo "Hukum / Fakultas Hukum";
            }
            if($alumni == 7){
                echo "Informatika / Fakultas Teknologi Komunikasi dan Informatika ";
            }
            else if($alumni == 8){
                echo "Sistem Informasi / Fakultas Teknologi Komunikasi dan Informatika ";
            }
            else if($alumni == 9){
              echo "Biologi / Fakultas Biologi";
            }
            else if($alumni == 10){
              echo "Agroteknologi / Fakultas Pertanian";
            }
            else if($alumni == 11){
              echo "Managemen / Fakultas Ekomnomi";
            }
            else if($alumni == 12){
              echo "Ekonomi / Fakultas Ekonomi";
            }
            else if($alumni == 13){
              echo "Pariwisata / Fakultas Ekonomi";
            }
            else if($alumni == 14){
              echo "Ilmu Keperawatan / Fakultas Ilmu Kesehatan";
            }
            else if($alumni == 15){
              echo "Bahasa Korea / Abarnas";
            }
            else if($alumni == 16){
              echo "Akparnas / Perhotelan";
            }
            else if($alumni == 17){
              echo "Teknik Elektro / Fakultas Teknik dan Sains";
            }
            else if($alumni == 18){
              echo "Teknik Mesin / Fakultas Teknik dan Sains";
            }
            else if($alumni == 19){
              echo "Teknik Fisika / Fakultas Teknik dan Sains";
            }
            else if($alumni == 20){
              echo "Fisika / Fakultas Teknik dan Sains ";
            }
            else if($alumni == 21){
              echo "Sastra Inggris / Fakultas Bahasa dan Sastra";
            }
            else if($alumni == 22){
              echo "Sastra Indonesia / Fakultas Bahasa dan Sastra";
            }
            else if($alumni == 23){
              echo "Sastra Jepang / Fakultas Bahasa dan Sastra";
            }
            else if($alumni == 24){
              echo "Bahasa Korea / Fakultas Bahasa dan Sastra ";
            }
           
          ?>
            
            
            </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>NPM</th>
                    <th>Name</th>
                    <th>Jenis Kelamin</th>
                    <th>Tahun Masuk</th>
                    <th>Tahun lulus</th>
                    <th>IPK</th>        
                    <th>Status</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tfoot>
                  
                </tfoot>
                <tbody>
                <?php
                  $alumni = $this->session->userdata('prodi');
                  
                  $cek_query=$this->UserModel->data_alumni($alumni); 
                  foreach ($cek_query->result_array() as $row)
                  {       
                ?>
                  <tr>
                    <td><?php echo $row['nim'] ; ?></td>
                    <td><?php echo $row['nama_alumni'] ; ?></td>
                    <td><?php echo $row['jenis_kelamin'] ; ?></td>
                    <td><?php echo $row['tahun_masuk'] ; ?></td>
                    <td><?php echo $row['tahun_lulus'] ; ?></td>
                    <td><?php echo $row['ipk'] ; ?></td>
                    <td><?php echo $row['status_bekerja'] ; ?></td>
                    <td>
                       <button type="button" class="btn btn-outline-danger"><a href="<?php echo base_url('index.php/Aksialumni/dta_delete?ni=') . $row['nim']; ?>"> Hapus data </a> </button>
                       <button type="button" class="btn btn-outline-warning"><a href="<?php echo base_url('index.php/Layout/vedit_data_survey_alumni?ni=') . $row['nim']; ?>"> Ubah data </a></button>
                       <button type="button" class="btn btn-outline-warning"><a href="<?php echo base_url('index.php/penghubung/vedit?ni=') . $row['nim']; ?>"> View data </a></button>
                    </td>
                  </tr>

                    <?php } ?>
                 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      
    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href=" <?php echo base_url("#page-top")?>">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href=" <?php echo base_url("login.html")?>">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url("vendor/jquery/jquery.min.js ")?>"></script>
  <script src="<?php echo base_url("vendor/bootstrap/js/bootstrap.bundle.min.js ")?>"></script>

  <!-- Core plugin JavaScript-->
  <script src=" <?php echo base_url("vendor/jquery-easing/jquery.easing.min.js")?>"></script>

  <!-- Page level plugin JavaScript-->
  <script src=" <?php echo base_url("vendor/chart.js/Chart.min.js")?>"></script>
  <script src="<?php echo base_url("vendor/datatables/jquery.dataTables.js ")?>"></script>
  <script src=" <?php echo base_url("vendor/datatables/dataTables.bootstrap4.js")?>"></script>

  <!-- Custom scripts for all pages-->
  <script src=" <?php echo base_url("js/sb-admin.min.js")?>"></script>

  <!-- Demo scripts for this page-->
  <script src="<?php echo base_url("js/demo/datatables-demo.js ")?>"></script>
  <script src=" <?php echo base_url("js/demo/chart-area-demo.js")?>"></script>

</body>

</html>
