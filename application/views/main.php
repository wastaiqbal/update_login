<!DOCTYPE html>
<html lang="en">

<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>Lab FTKI UNAS</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- styles -->
  <link href=" <?php echo base_url("assets/css/bootstrap.css")?>" rel="stylesheet">
  <link href=" <?php echo base_url("assets/css/bootstrap-responsive.css")?>" rel="stylesheet">
  <link href="" rel="stylesheet <?php echo base_url("assets/css/docs.css")?>">
  <link href="" rel="stylesheet <?php echo base_url("assets/css/prettyPhoto.css")?>">
  <link href=" <?php echo base_url("assets/js/google-code-prettify/prettify.css")?>" rel="stylesheet">
  <link href=" <?php echo base_url("assets/css/camera.css")?>" rel="stylesheet ">
  <link href=" <?php echo base_url("https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,300|Open+Sans:400,300,300italic,400italic")?>" rel="stylesheet">
  <link href=" <?php echo base_url("assets/css/style.css")?>" rel="stylesheet">
  <link href=" <?php echo base_url("assets/color/success.css")?>" rel="stylesheet">

  <!-- fav and touch icons -->
  <link rel="shortcut icon" href="assets/ico/favicon.ico">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href=" <?php echo base_url("assets/ico/apple-touch-icon-144-precomposed.png")?>">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href=" <?php echo base_url("assets/ico/apple-touch-icon-114-precomposed.png")?>">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href=" <?php echo base_url("assets/ico/apple-touch-icon-72-precomposed.png")?>">
  <link rel="apple-touch-icon-precomposed" href=" <?php echo base_url("assets/ico/apple-touch-icon-57-precomposed.png")?>">

  <!-- =======================================================
    Theme Name: Scaffold
    Theme URL: https://bootstrapmade.com/scaffold-bootstrap-metro-style-template/
    Author: BootstrapMade.com
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>

  <header>
    <!-- Navbar
    ================================================== -->
    <div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
          <!-- logo -->
          <a class="brand logo" href=" <?php echo base_url("index.html")?>">
            <img src=" <?php echo base_url("assets/img/logounas.jpg")?>" alt="" />
          </a>
          <!-- end logo -->

          <!-- top menu -->
          
          <!-- end menu -->
        </div>
      </div>
    </div>
  </header>
  <?php
 $this->session->unset_userdata('lab');

?>
  <section id="intro">
    <div class="jumbotron masthead">

      <div class="container">
        <div class="row">
          <div class="span12">
            <div class="camera_wrap camera_black_skin" id="camera_wrap_2">
              <div data-thumb=" <?php echo base_url("assets/img/slides/camera/img1.jpeg")?>" data-src=" <?php echo base_url("assets/img/slides/camera/img1.jpg")?>">
                <div class="camera_caption fadeFromBottom">
                  <h2>Laboratorium FTKI</h2>
                  <div class="hidden-phone">
                    <p>
                      Laboratorium FTKI terdiri dari 4 labratorium, antara lain E-Commerce, NDC, Multimedia dan Commputer Vision, Artificial Intelegent.
                    </p>
                  </div>
                </div>
              </div>
              <div data-thumb=" <?php echo base_url("assets/img/slides/camera/img2.jpeg")?>" data-src="<?php echo base_url("assets/img/slides/camera/img2.jpg ")?>">
                <div class="camera_caption fadeFromBottom">
                  <h2>Fungsi Utama</h2>
                  <div class="hidden-phone">
                    <p>
                      Sebagai sarana belajar bagi mahasiswa Universitas Nasional khususnya mahasiswa Fakultas Teknologi Komunikasi dan Informatika.
                    </p>
                  </div>
                </div>
              </div>
              <div data-thumb=" <?php echo base_url("assets/img/slides/camera/img3.jpeg")?>" data-src=" <?php echo base_url("assets/img/slides/camera/img3.jpg")?>">
                <div class="camera_caption fadeFromBottom">
                  <h2>Fungsi Lain</h2>
                  <div class="hidden-phone">
                    <p>
                      Sebagai tempat melakukan kegiatan seperti pelatihan, sertifikasi, form diskusi, dan lain sebagainya.
                    </p>
                  </div>
                </div>
              </div>
              <div data-thumb=" <?php echo base_url("assets/img/slides/camera/img4.jpeg")?>" data-src="assets/img/slides/camera/img4.jpg">
                <div class="camera_caption fadeFromBottom">
                  <h2>Komitmen</h2>
                  <div class="hidden-phone">
                    <p>
                      Kami selaku pengurus lab ftki berkomitmen mendukung seluruh kegiatan positif yang dilakukan oleh ftki baik internal atau ekternal.
                    </p>
                  </div>
                </div>
              </div>
            </div>
            <!-- #camera_wrap_1 -->

          </div>
        </div>
      </div>
    </div>
  </section>



  <!-- end tagline -->

  <section id="maincontent">
    <div class="container">

      <div class="row">
        <div class="span3">
        
         <a href="<?php echo base_url("index.php/Layout/form_login")?>"><h3 class="heading-success"><span class="btn btn-large btn-success"><i class="m-icon-big-swapright m-icon-white"></i></span>&nbsp;&nbsp;Login</h3></a>
          <p>
          <?php
            date_default_timezone_set('Asia/Jakarta');
            echo  date("l, j F Y, H:i"); 
          ?>
           
           <?php
            
           ?>
          </p>
        </div>
        <div class="span3">
          <div class="well well-primary box">
            <img src=" <?php echo base_url("assets/img/icons/box-1-white.png")?>" alt="" />
            <h3>Recruitment</h3>
            <p>
            Jadilah bagian dari kami
            </p>
            
            <?php
            $tgl_sekarang=date("Y-m-d");//tanggal sekarang
            $tgl_mulai=date("Y")."-06-15";// tanggal launching aplikasi
            $jangka_waktu = strtotime('+30 days', strtotime($tgl_mulai));// jangka waktu + 365 hari
            $tgl_exp=date("Y-m-d",$jangka_waktu);//tanggal expired
            if (($tgl_sekarang >= $tgl_exp ) || ($tgl_sekarang <= $tgl_mulai))
            {
            ?> 
              <a href="<?php echo base_url("index.php/Layout/caslab ")?>">Read more</a>
            <?php
            } 
            else {
            ?>
              <a href="<?php echo base_url("index.php/Layout/caslab ")?>">Read more</a>
            <?php }?>
                        
            

           
           
             
          </div>
        </div>
        <div class="span3">
          <div class="well well-success box">
            <img src=" <?php echo base_url("assets/img/icons/box-2-white.png")?>" alt="" />
            <h3>Modul</h3>
            <p>
              Klik Disini untuk mengakses modul
            </p>
            <a href="<?php echo base_url("index.php/Layout/visitor_modul ")?>">Read more</a>
          </div>
        </div>
        <div class="span3">
          <div class="well well-warning box">
            <img src=" <?php echo base_url("assets/img/icons/box-3-white.png")?>" alt="" />
            <h3>Bantuan</h3>
            <p>
              Jika mengalami kesulitan dan kendala, harap hubungi asisten laboratorium
            </p>
            
          </div>
        </div>
      </div>

    </div>
  </section>


  <section id="bottom">
    <div class="container">

      <!-- divider -->
      <div class="row">
        <div class="span12">
          <div class="divider"></div>
        </div>
      </div>
      <!-- end divider -->


  <!-- Footer
 ================================================== -->
  <footer class="footer">
    <div class="container">
      <div class="row">
        <div class="span4">
          <div class="widget">
            <h4>About us</h4>
            <address>
					<strong>Lab FTKI Universitas Nasional</strong><br>
					Lt.4 Blok 4 dan Selasar Lt.4 Universitas Nasional<br>
					Jl. Sawo Manila, RT.14/RW.3, Ps. Minggu, Kec. Ps. Minggu, Kota Jakarta Selatan, Daerah Khusus Ibukota Jakarta 12520<br>
					
					</address>

            
          </div>
        </div>
        <div class="span4">
          <div class="widget">
            <h4>Browse pages</h4>
            <ul class="nav nav-list regular">
              <li class="nav-header">More from us</li>
              <li><a href="https://www.unas.ac.id/">Universitas Nasional</a></li>
              <li><a href="http://ti.ftki.unas.ac.id/">Teknik Informatika - Unas</a></li>
              <li><a href="http://si.ftki.unas.ac.id/">Sistem Informasi - Unas</a></li>
              <li class="nav-header">Quick links</li>
              <li><a href="http://webkuliah.unas.ac.id/">Web Kuliah Unas</a></li>
              <li><a href="http://apps.unas.ac.id/">Akademik Online Unas</a></li>
            </ul>
          </div>
        </div>
        <div class="span4">
          <div class="widget">
            <h4>About Website</h4>
            <form class="form-horizontal" action="<?php echo base_url(" ")?>" method="post">
              <fieldset>
                <p>
                  Dibagun sebagai sarana pendukung kegiatan laboratorium FTKI UNAS
                </p>
                <p>
                  Dibuat oleh Danang (Aslab E-Commerce/Info 1) menggunakan framework Codeigniter
                </p>

               
              </fieldset>
            </form>
           
          </div>
        </div>
      </div>
    </div>

    <div class="verybottom">
      <div class="container">
        <div class="row">
          <div class="span6">
            <p>&copy; Tamplate : Scaffold - All right reserved</p>
            <p>&copy; Dibuat oleh Danang Aji Pangestu (Aslab E-Com/Info 1) Mahasiswa Informatika 2017</p>
          </div>
          <div class="span6">
            <div class="pull-right">
              <div class="credits">
                <!--
                  All the links in the footer should remain intact.
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Scaffold
                -->
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </footer>

  <script src=" <?php echo base_url("assets/js/jquery-1.8.2.min.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/jquery.easing.1.3.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/google-code-prettify/prettify.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/modernizr.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/bootstrap.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/jquery.elastislide.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/jquery.flexslider.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/jquery.prettyPhoto.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/application.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/hover/jquery-hover-effect.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/hover/setting.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/camera/camera.min.js")?>"></script>
  <script src=" <?php echo base_url("assets/js/camera/setting.js")?>"></script>

  <!-- Template Custom JavaScript File -->
  <script src="<?php echo base_url("assets/js/custom.js ")?>"></script>

</body>

</html>
