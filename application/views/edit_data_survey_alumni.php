<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Data Alumni Unas</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url("vendor/fontawesome-free/css/all.min.css ")?>" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="<?php echo base_url("vendor/datatables/dataTables.bootstrap4.css ")?>" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href=" <?php echo base_url("css/sb-admin.css")?>" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="index.html">Start Bootstrap</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="<?php echo base_url("#")?>">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
      <div class="input-group">
        <div class="input-group-append">
        <div style="margin-top: 50px;" color="white"><center>
        
        
        
        </center></div>
        </div>
      </div>
    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-bell fa-fw"></i>
          <span class="badge badge-danger">9+</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown">
          <a class="dropdown-item" href="<?php echo base_url("")?>">Action</a>
          <a class="dropdown-item" href="<?php echo base_url("")?>">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url("")?>">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="<?php echo base_url("")?>" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-envelope fa-fw"></i>
          <span class="badge badge-danger">7</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="messagesDropdown">
          <a class="dropdown-item" href="<?php echo base_url("")?>"><?php echo $this->session->userdata('nama_dosen'); ?></a>
          <a class="dropdown-item" href="<?php echo base_url("")?>">Another action</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url("")?>">Something else here</a>
        </div>
      </li>
      <li class="nav-item dropdown no-arrow">
        <a class="nav-link dropdown-toggle" href="<?php echo base_url("")?>" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <i class="fas fa-user-circle fa-fw"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
          <a class="dropdown-item" href="<?php echo base_url("")?>"><?php echo $this->session->userdata('nama_dosen'); ?></a>
          <a class="dropdown-item" href="<?php echo base_url("")?>">Activity Log</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="<?php echo base_url("")?>" data-toggle="modal" data-target="#logoutModal">Logout</a>
        </div>
      </li>
    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url("index.php/Layout/menu")?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span>
        </a>
      </li>
      
      
      <li class="nav-item">
        <a class="nav-link" href=" <?php echo base_url('index.php/Layout/view')  ?>">
          <i class="fas fa-fw fa-table"></i>
          <span>Tables</span> </a>
      </li>
    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a>
            <?php
            $alumni = $this->session->userdata('prodi');
            if($alumni == 1){
                echo "Ilmu Politik / Fakultas Ilmu Sosial dan Politik";
            }
            else if($alumni == 2){
                echo "Administrasi Negara / Fakultas Ilmu Sosial dan Politik";
            }
            else if($alumni == 3){
                echo "Hubungan Internasional / Fakultas Ilmu Sosial dan Politik";
            }
            else if($alumni == 4){
                echo "Sosiologi / Fakultas Ilmu Sosial dan Politik";
            }
            else if($alumni == 5){
                echo "Ilmu Komunikasi / Fakultas Ilmu Sosial dan Politik";
            }
            else if($alumni == 6){
                echo "Hukum / Fakultas Hukum";
            }
            else if($alumni == 7){
                echo "Informatika / Fakultas Teknologi Komunikasi dan Informatika ";
            }
            else if($alumni == 8){
                echo "Sistem Informasi / Fakultas Teknologi Komunikasi dan Informatika ";
            }
            else if($alumni == 9){
              echo "Biologi / Fakultas Biologi";
            }
            else if($alumni == 10){
              echo "Agroteknologi / Fakultas Pertanian";
            }
            else if($alumni == 11){
              echo "Managemen / Fakultas Ekomnomi";
            }
            else if($alumni == 12){
              echo "Ekonomi / Fakultas Ekonomi";
            }
            else if($alumni == 13){
              echo "Pariwisata / Fakultas Ekonomi";
            }
            else if($alumni == 14){
              echo "Ilmu Keperawatan / Fakultas Ilmu Kesehatan";
            }
            else if($alumni == 15){
              echo "Bahasa Korea / Abarnas";
            }
            else if($alumni == 16){
              echo "Akparnas / Perhotelan";
            }
            else if($alumni == 17){
              echo "Teknik Elektro / Fakultas Teknik dan Sains";
            }
            else if($alumni == 18){
              echo "Teknik Mesin / Fakultas Teknik dan Sains";
            }
            else if($alumni == 19){
              echo "Teknik Fisika / Fakultas Teknik dan Sains";
            }
            else if($alumni == 20){
              echo "Fisika / Fakultas Teknik dan Sains ";
            }
            else if($alumni == 21){
              echo "Sastra Inggris / Fakultas Bahasa dan Sastra";
            }
            else if($alumni == 22){
              echo "Sastra Indonesia / Fakultas Bahasa dan Sastra";
            }
            else if($alumni == 23){
              echo "Sastra Jepang / Fakultas Bahasa dan Sastra";
            }
            else if($alumni == 24){
              echo "Bahasa Korea / Fakultas Bahasa dan Sastra ";
            }
           


          ?>
            </a>
          </li>
          
        </ol>

        <!-- Icon Cards-->
        <div class="row">
        
        </div>

        <!-- Area Chart Example-->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-chart-area"></i>
            Edit data survey alumni</div>
          <div class="card-body">
          <div class="row">
          <form action="<?php echo base_url('index.php/Aksialumni/dta_edit') ?>" method="post" enctype="multipart/form-data" autocomplete="off">
	<div class="container">
            <?php

            $nim = $this->input->get('ni');
            $cek=$this->Dataalumni->cek_data_alumni($nim);

            foreach ($cek->result_array() as $row)
            {  
                
                
            ?>
        <div class="form-group row">
            <label><h5> Kuesioner untuk Alumni (Trace Study) </h5></label>
          
            
            
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Nomor Pokok Mahasiswa (NPM)</label>
			<div class="form-group col-sm-8">	
				<input type="text" name="nim" class="form-control" value="<?php echo $row['nim'] ?>" >
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Nama dan Gelar</label>
			<div class="form-group col-sm-8">	
				<input type="text" name="nama" class="form-control" value="<?php echo $row['nama_alumni'] ?>" >
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label"> Prodi / Fakultas </label>
			<div class="col-sm-8">
                <select class="form-control" name="prodi" required >
                    <option <?php echo ($row['prodi'] == '1') ? "selected": "" ?>> Ilmu Politik / Fakultas Ilmu Sosial dan Politik </option>
                    <option <?php echo ($row['prodi'] == '2') ? "selected": "" ?>> Administrasi Negara / Fakultas Ilmu Sosial dan Politik </option>
                    <option <?php echo ($row['prodi'] == '3') ? "selected": "" ?>> Hubungan Internasional / Fakultas Ilmu Sosial dan Politik </option>
                    <option <?php echo ($row['prodi'] == '4') ? "selected": "" ?>> Sosiologi / Fakultas Ilmu Sosial dan Politik </option>
                    <option <?php echo ($row['prodi'] == '5') ? "selected": "" ?>> Ilmu Komunikasi / Fakultas Ilmu Sosial dan Politik </option>
                    <option <?php echo ($row['prodi'] == '6') ? "selected": "" ?>> Hukum / Fakultas Hukum  </option>
                    <option <?php echo ($row['prodi'] == '7') ? "selected": "" ?>> Informatika / Fakultas Teknologi Komunikasi dan Informatika </option>
                    <option <?php echo ($row['prodi'] == '8') ? "selected": "" ?>> Sistem Informasi / Fakultas Teknologi Komunikasi dan Informatika </option>
                    <option <?php echo ($row['prodi'] == '9') ? "selected": "" ?>> Biologi / Fakultas Biologi </option>
                    <option <?php echo ($row['prodi'] == '10') ? "selected": "" ?>>Agroteknologi / Fakultas Pertanian </option>
                    <option <?php echo ($row['prodi'] == '11') ? "selected": "" ?>>Managemen / Fakultas Ekomnomi </option>
                    <option <?php echo ($row['prodi'] == '12') ? "selected": "" ?>>Ekonomi / Fakultas Ekonomi </option>
                    <option <?php echo ($row['prodi'] == '13') ? "selected": "" ?>>Pariwisata / Fakultas Ekonomi </option>
                    <option <?php echo ($row['prodi'] == '14') ? "selected": "" ?>>Ilmu Keperawatan / Fakultas Ilmu Kesehatan </option>
                    <option <?php echo ($row['prodi'] == '15') ? "selected": "" ?>>Bahasa Korea / Abarnas </option>
                    <option <?php echo ($row['prodi'] == '16') ? "selected": "" ?>>Akparnas / Perhotelan  </option>
                    <option <?php echo ($row['prodi'] == '17') ? "selected": "" ?>> Teknik Elektro / Fakultas Teknik dan Sains </option>
                    <option <?php echo ($row['prodi'] == '18') ? "selected": "" ?>> Teknik Mesin / Fakultas Teknik dan Sains</option>
                    <option <?php echo ($row['prodi'] == '19') ? "selected": "" ?>> Teknik Fisika / Fakultas Teknik dan Sains  </option>
                    <option <?php echo ($row['prodi'] == '20') ? "selected": "" ?>> Fisika / Fakultas Teknik dan Sains </option>
                    <option <?php echo ($row['prodi'] == '21') ? "selected": "" ?>>  Sastra Inggris / Fakultas Bahasa dan Sastra  </option>
                    <option <?php echo ($row['prodi'] == '22') ? "selected": "" ?>>  Sastra Indonesia / Fakultas Bahasa dan Sastra </option>
                    <option <?php echo ($row['prodi'] == '23') ? "selected": "" ?>>  Sastra Jepang / Fakultas Bahasa dan Sastra  </option>
                    <option <?php echo ($row['prodi'] == '24') ? "selected": "" ?>>Bahasa Korea / Fakultas Bahasa dan Sastra  </option>
     

                </select>
			</div>
		</div>
        <div class="form-group row">
            <label  class="col-sm-3 col-form-label">Jenis Kelamin</label>
			<div class="form-group col-sm-8">	   
                    <input type="radio" name="jenis_kelamin" value="L" <?php echo ($row['jenis_kelamin'] == 'L') ? "checked": "" ?> > Laki-laki <br>
                    <input type="radio" name="jenis_kelamin" value="P" <?php echo ($row['jenis_kelamin'] == 'P') ? "checked": "" ?> > Perempuan <br>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Nomer telepon</label>
			<div class="form-group col-sm-8" >	
				<input type="text" name="tpn" class="form-control" value="<?php echo $row['telepon'] ?>" required >
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Apakah anda sudah bekerja?</label>
			<div class="form-group col-sm-8">	
                    <input type="radio" name="pekerjaan" value="Belum_bekerja" <?php echo ($row['status_bekerja'] == 'Belum') ? "checked": "" ?> > Sudah <br>
                    <input type="radio" name="pekerjaan" value="sudah_bekerja" <?php echo ($row['status_bekerja'] == 'Sudah') ? "checked": "" ?> > Belum <br>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label"> Tahun Masuk </label>
			<div class="col-sm-8">
                <select class="form-control" name="tahun_masuk" required >
                    <option> <?php echo $row['tahun_masuk'] ?> </option>
                    <?php 
                         $tahun=getdate();
                         $y = $tahun['year']; 
                         
                        for ($n=1949; $n <= $y ; $n++) { ?>
                            <option value="<?php echo $n; ?>" > <?php echo $n; ?> </option>
                    <?php } ?>
                </select>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label"> Tahun Lulus </label>
			<div class="col-sm-8">
                <select class="form-control" name="tahun_lulus" >
                    <option> <?php echo $row['tahun_lulus'] ?> </option>
                    <?php 
                         $tahun=getdate();
                         $y = $tahun['year']; 
                         
                        for ($n=1949; $n <= $y ; $n++) { ?>
                            <option value="<?php echo $n; ?>" > <?php echo $n; ?> </option>
                    <?php } ?>
                </select>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Indeks Prestasi Komulatif (IPK)</label>
			<div class="form-group col-sm-8">	
				<input type="text" name="ipk" class="form-control"  value="<?php echo $row['ipk'] ?>" required >
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Nama Perusahaan</label>
			<div class="form-group col-sm-8">	
				<input type="text" name="nama_perusahaan"  value="<?php echo $row['nama_perusahaan'] ?>"  class="form-control">
			</div>
		</div>
        
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Alamat Perusahaan</label>
			<div class="form-group col-sm-8">	
                <textarea name="alamat_perusahaan"  value="<?php echo $row['alamat_perusahaan'] ?>" class="form-control" cols="40" rows="4"></textarea>
			</div>
		</div>

        <div class="form-group row">
			<label class="col-sm-3 col-form-label"> Tahun bekerja </label>
			<div class="col-sm-8">
                <select class="form-control" name="tahun_bekerja" >
                <option> <?php echo $row['tahun_bekerja'] ?> </option>
                    <?php 
                         $tahun=getdate();
                         $y = $tahun['year']; 
                         
                        for ($n=1949; $n <= $y ; $n++) { ?>
                            <option value="<?php echo $n; ?>" > <?php echo $n; ?> </option>
                    <?php } ?>
                </select>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label"> Sallery </label>
			<div class="col-sm-8">
                <select class="form-control" name="sallery" >
                    <option <?php echo ($row['sallery'] == '<Rp.1000.0000') ? "selected": "" ?> > < Rp.1000.0000 </option>
                    <option <?php echo ($row['sallery'] == 'Rp.1000.000-Rp.3000.000') ? "selected": "" ?> > Rp.1000.000-Rp.3000.000 </option>
                    <option <?php echo ($row['sallery'] == 'Rp.3000.000-Ro.5000.000') ? "selected": "" ?> > Rp.3000.000-Ro.5000.000 </option>
                    <option <?php echo ($row['sallery'] == 'Rp.5000.000-Rp.7000.000') ? "selected": "" ?> > Rp.5000.000-Rp.7000.000 </option>
                    <option <?php echo ($row['sallery'] == 'Rp.7000.000-Rp.10.000.000') ? "selected": "" ?> > Rp.7000.000-Rp.10.000.000 </option>
                    <option <?php echo ($row['sallery'] == '>Rp10.000.000') ? "selected": "" ?> > Informatika / > Rp10.000.000 </option>
                </select>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Jabatan</label>
			<div class="form-group col-sm-8">	
                <textarea name="jabatan_perusahaan" class="form-control" cols="40" rows="4"></textarea>
			</div>
		</div>		
        <label><h5> 1. Bidang Kelimuan </h5></label>
        <div class="form-group row">
			<div class="form-group col-sm-8">	
            <label>1.1. Apakah pekerjaan anda sesuai dengan bidang keilmuan ?</label><br>
                    <input type="radio" name="idm1" value="sudah_sesuai" <?php echo ($row['idm1'] == 'Sudah') ? "checked": "" ?> > Sudah <br>
                    <input type="radio" name="idm1" value="belum_sesuai" <?php echo ($row['idm1'] == 'Belum') ? "checked": "" ?>> Belum <br>
			</div>
            <div class="form-group col-sm-8">	
            <label>1.2. Seberapa  pengaruhnya bidang keilmuan terhadap pekerjaan (skala 1 s/d 5) </label><br>
                    <input type="radio" name="idm2" value="1" <?php echo ($row['idm2'] == '1') ? "checked": "" ?> > 1 <br>
                    <input type="radio" name="idm2" value="2" <?php echo ($row['idm2'] == '2') ? "checked": "" ?> > 2 <br>
                    <input type="radio" name="idm2" value="3" <?php echo ($row['idm2'] == '3') ? "checked": "" ?> > 3 <br>
                    <input type="radio" name="1dm2" value="4" <?php echo ($row['idm2'] == '4') ? "checked": "" ?> > 4 <br>
                    <input type="radio" name="idm2" value="5" <?php echo ($row['idm2'] == '5') ? "checked": "" ?> > 5 <br>
			</div>
		</div>
        
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Jika anda belum bekerja, apa penyebabnya?</label>
			<div class="form-group col-sm-8">	
                <textarea name="alasan_belum_bekerja" class="form-control" cols="40" rows="4"></textarea>
			</div>
		</div>

        <label><h5> 2. Keahlian berdasarkan bidang ilmu (Profesionalisme) </h5></label>
        <div class="form-group row">
			<div class="form-group col-sm-8">	
            <label>2.1. Apakah alumni mampu menerapkan keahlian atau bidang ilmu sesuai dengan kebutuhan perusahaan?</label><br>
                    <input type="radio" name="profesi" value="SK" <?php echo ($row['profesi'] == 'SK') ? "checked": "" ?> > Sangat kurang <br>
                    <input type="radio" name="profesi" value="K" <?php echo ($row['profesi'] == 'K') ? "checked": "" ?> > Kurang <br>
                    <input type="radio" name="profesi" value="C" <?php echo ($row['profesi'] == 'C') ? "checked": "" ?> > Cukup <br>
                    <input type="radio" name="profesi" value="B" <?php echo ($row['profesi'] == 'B') ? "checked": "" ?> > Baik <br>
                    <input type="radio" name="profesi" value="SB" <?php echo ($row['profesi'] == 'SB') ? "checked": "" ?> > Sangat baik <br>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Masukan untuk Universitas, Prodi, dan Fakultas (Uraian Kalimat)</label>
			<div class="form-group col-sm-8">	
                <textarea name="masukan" class="form-control" cols="40" rows="4"> <?php echo $row['masukan'] ?>"</textarea>
			</div>
		</div>
       
		<div class="form-group row">
		<div class="col-sm-11" style="float: right;">	
				<button class="btn btn-lg btn-danger btn-block" name="batal">BATAL</button>
				<button class="btn btn-lg btn-primary btn-block" name="tambah">TAMBAH</button>
				<a href="<?php echo base_url("index.php/Layout/view")?>" class="btn btn-lg btn-dark btn-block">KEMBALI</a>
		</div>	
		</div>
    </div>
    <?php } ?>
</form>

        </div>

          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

      

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Data Tracker Alumni Create by Danang 173112706450221</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href=" <?php echo base_url("#page-top")?>">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href=" <?php echo base_url("login.html")?>">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url("vendor/jquery/jquery.min.js ")?>"></script>
  <script src="<?php echo base_url("vendor/bootstrap/js/bootstrap.bundle.min.js ")?>"></script>

  <!-- Core plugin JavaScript-->
  <script src=" <?php echo base_url("vendor/jquery-easing/jquery.easing.min.js")?>"></script>

  <!-- Page level plugin JavaScript-->
  <script src=" <?php echo base_url("vendor/chart.js/Chart.min.js")?>"></script>
  <script src="<?php echo base_url("vendor/datatables/jquery.dataTables.js ")?>"></script>
  <script src=" <?php echo base_url("vendor/datatables/dataTables.bootstrap4.js")?>"></script>

  <!-- Custom scripts for all pages-->
  <script src=" <?php echo base_url("js/sb-admin.min.js")?>"></script>

  <!-- Demo scripts for this page-->
  <script src="<?php echo base_url("js/demo/datatables-demo.js ")?>"></script>
  <script src=" <?php echo base_url("js/demo/chart-area-demo.js")?>"></script>

</body>

</html>
