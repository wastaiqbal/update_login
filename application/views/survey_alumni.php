

<!-- FORM -->


<!DOCTYPE html>
<html lang="en">
<head>
	<title>Survey Pengguna Alumni</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href=" <?php echo base_url("tinputsurvey/images/icons/favicon.ico")?>"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" <?php echo base_url("tinputsurvey/vendor/bootstrap/css/bootstrap.min.css")?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" <?php echo base_url("tinputsurvey/fonts/font-awesome-4.7.0/css/font-awesome.min.css")?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("tinputsurvey/fonts/Linearicons-Free-v1.0.0/icon-font.min.css")?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("tinputsurvey/vendor/animate/animate.css")?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("tinputsurvey/vendor/css-hamburgers/hamburgers.min.css")?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("tinputsurvey/vendor/select2/select2.min.css")?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("tinputsurvey/css/util.css")?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("tinputsurvey/css/main.css")?>">
<!--===============================================================================================-->
</head>
<body>



<div class="container-contact100" style="background-image:  url('<?php echo base_url("tinputsurvey/images1/bg-01.jpg")?>');">
		<div class="wrap-contact100">
	<form action="<?php echo base_url('index.php/Penghubung/survey_alumni') ?>" method="post" enctype="multipart/form-data" autocomplete="off">
	<div class="container">
        
        <div class="form-group row">
            <label><h5> Kuesioner untuk Alumni (Trace Study) </h5></label>
            <label>Dengan hormat,<br>
            Kami ucapkan salam kepada Alumni atas ketersediaan untuk mengisi kuesioner berikut.
            Survey ini ditujukan untuk untuk Alumni sebagai pengguna alumni lulusan Universitas Nasioanl.
            Informasi yang Anda berikan kepada kami dapat bermanfaat dan menjadi evaluasi bagi kami agar lebih baik.<br>
            Atas perhatiannya kami ucapkan terimakasih

            </label><br>
            
            
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Nomor Pokok Mahasiswa (NPM)</label>
			<div class="form-group col-sm-8">	
				<input type="text" name="nim" class="form-control" required >
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Nama dan Gelar</label>
			<div class="form-group col-sm-8">	
				<input type="text" name="nama" class="form-control" required >
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label"> Prodi / Fakultas </label>
			<div class="col-sm-8">
                <select class="form-control" name="prodi" required >
                <option value="1"> Ilmu Politik / Fakultas Ilmu Sosial dan Politik </option>
                    <option value="2"> Administrasi Negara / Fakultas Ilmu Sosial dan Politik </option>
                    <option value="3"> Hubungan Internasional / Fakultas Ilmu Sosial dan Politik </option>
                    <option value="4"> Sosiologi / Fakultas Ilmu Sosial dan Politik </option>
                    <option value="5"> Ilmu Komunikasi / Fakultas Ilmu Sosial dan Politik </option>
                    <option value="6"> Hukum / Fakultas Hukum </option>
                    <option value="7"> Informatika / Fakultas Teknologi Komunikasi dan Informatika </option>
                    <option value="8"> Sistem Informasi / Fakultas Teknologi Komunikasi dan Informatika  </option>
                    <option value="9"> Biologi / Fakultas Biologi </option>
                    <option value="10"> Agroteknologi / Fakultas Pertanian </option>
                    <option value="11"> Managemen / Fakultas Ekomnomi </option>
                    <option value="12"> Ekonomi / Fakultas Ekonomi </option>
                    <option value="13"> Pariwisata / Fakultas Ekonomi </option>
                    <option value="14"> Ilmu Keperawatan / Fakultas Ilmu Kesehatan </option>
                    <option value="15"> Bahasa Korea / Abarnas </option>
                    <option value="16"> Akparnas / Perhotelan </option>
                    <option value="17"> Teknik Elektro / Fakultas Teknik dan Sains </option>
                    <option value="18"> Teknik Mesin / Fakultas Teknik dan Sains </option>
                    <option value="19"> Teknik Fisika / Fakultas Teknik dan Sains </option>
                    <option value="20"> Fisika / Fakultas Teknik dan Sains </option>
                    <option value="21"> Sastra Inggris / Fakultas Bahasa dan Sastra </option>
                    <option value="22"> Sastra Indonesia / Fakultas Bahasa dan Sastra </option>
                    <option value="23"> Sastra Jepang / Fakultas Bahasa dan Sastra </option>
                    <option value="24"> Bahasa Korea / Fakultas Bahasa dan Sastra </option>
                </select>
			</div>
		</div>
        <div class="form-group row">
            <label  class="col-sm-3 col-form-label">Jenis Kelamin</label>
			<div class="form-group col-sm-8">	   
                    <input type="radio" name="jenis_kelamin" value="L" > Laki-laki <br>
                    <input type="radio" name="jenis_kelamin" value="P" > Perempuan <br>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Nomer telepon</label>
			<div class="form-group col-sm-8" >	
				<input type="text" name="tpn" class="form-control" required >
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Apakah anda sudah bekerja?</label>
			<div class="form-group col-sm-8">	
                    <input type="radio" name="pekerjaan" value="Belum" > Belum <br>
                    <input type="radio" name="pekerjaan" value="Sudah" > Sudah <br>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label"> Tahun Masuk </label>
			<div class="col-sm-8">
                <select class="form-control" name="tahun_masuk" required >
                    <?php 
                         $tahun=getdate();
                         $y = $tahun['year']; 
                         
                        for ($n=1949; $n <= $y ; $n++) { ?>
                            <option value="<?php echo $n; ?>" > <?php echo $n; ?> </option>
                    <?php } ?>
                </select>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label"> Tahun Lulus </label>
			<div class="col-sm-8">
                <select class="form-control" name="tahun_lulus" >
                    <?php 
                         $tahun=getdate();
                         $y = $tahun['year']; 
                         
                        for ($n=1949; $n <= $y ; $n++) { ?>
                            <option value="<?php echo $n; ?>" > <?php echo $n; ?> </option>
                    <?php } ?>
                </select>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Indeks Prestasi Komulatif (IPK)</label>
			<div class="form-group col-sm-8">	
				<input type="text" name="ipk" class="form-control" required >
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Nama Perusahaan</label>
			<div class="form-group col-sm-8">	
				<input type="text" name="nama_perusahaan" class="form-control">
			</div>
		</div>
        
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Alamat Perusahaan</label>
			<div class="form-group col-sm-8">	
                <textarea name="alamat_perusahaan" class="form-control" cols="40" rows="4"></textarea>
			</div>
		</div>

        <div class="form-group row">
			<label class="col-sm-3 col-form-label"> Tahun bekerja </label>
			<div class="col-sm-8">
                <select class="form-control" name="tahun_bekerja" >
                    <?php 
                         $tahun=getdate();
                         $y = $tahun['year']; 
                         
                        for ($n=1949; $n <= $y ; $n++) { ?>
                            <option value="<?php echo $n; ?>" > <?php echo $n; ?> </option>
                    <?php } ?>
                </select>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label"> Sallery </label>
			<div class="col-sm-8">
                <select class="form-control" name="sallery" >
                    <option value="<Rp.1000.0000"> < Rp.1000.0000 </option>
                    <option value="Rp.1000.000-Rp.3000.000"> Rp.1000.000-Rp.3000.000 </option>
                    <option value="Rp.3000.000-Ro.5000.000"> Rp.3000.000-Ro.5000.000 </option>
                    <option value="Rp.5000.000-Rp.7000.000"> Rp.5000.000-Rp.7000.000 </option>
                    <option value="Rp.7000.000-Rp.10.000.000"> Rp.7000.000-Rp.10.000.000 </option>
                    <option value=">Rp10.000.000"> Informatika / > Rp10.000.000 </option>
                </select>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Jabatan</label>
			<div class="form-group col-sm-8">	
                <textarea name="jabatan_perusahaan" class="form-control" cols="40" rows="4"></textarea>
			</div>
		</div>		
        <label><h5> 1. Bidang Kelimuan </h5></label>
        <div class="form-group row">
			<div class="form-group col-sm-8">	
            <label>1.1. Apakah pekerjaan anda sesuai dengan bidang keilmuan ?</label><br>
                    <input type="radio" name="idm1" value="sudah_sesuai" > sudah <br>
                    <input type="radio" name="idm1" value="belum_sesuai" > belum <br>
			</div>
            <div class="form-group col-sm-8">	
            <label>1.2. Seberapa  pengaruhnya bidang keilmuan terhadap pekerjaan (skala 1 s/d 5) </label><br>
                    <input type="radio" name="idm2" value="1" > 1 <br>
                    <input type="radio" name="idm2" value="2" > 2 <br>
                    <input type="radio" name="idm2" value="3" > 3 <br>
                    <input type="radio" name="1dm2" value="4" > 4 <br>
                    <input type="radio" name="idm2" value="5" > 5 <br>
			</div>
		</div>
        
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Jika anda belum bekerja, apa penyebabnya?</label>
			<div class="form-group col-sm-8">	
                <textarea name="alasan_belum_bekerja" class="form-control" cols="40" rows="4"></textarea>
			</div>
		</div>

        <label><h5> 2. Keahlian berdasarkan bidang ilmu (Profesionalisme) </h5></label>
        <div class="form-group row">
			<div class="form-group col-sm-8">	
            <label>2.1. Apakah alumni mampu menerapkan keahlian atau bidang ilmu sesuai dengan kebutuhan perusahaan?</label><br>
                    <input type="radio" name="profesi" value="SK" > Sangat kurang <br>
                    <input type="radio" name="profesi" value="K" > Kurang <br>
                    <input type="radio" name="profesi" value="C" > Cukup <br>
                    <input type="radio" name="profesi" value="B" > Baik <br>
                    <input type="radio" name="profesi" value="SB" > Sangat baik <br>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Masukan untuk Universitas, Prodi, dan Fakultas (Uraian Kalimat)</label>
			<div class="form-group col-sm-8">	
                <textarea name="masukan" class="form-control" cols="40" rows="4"></textarea>
			</div>
		</div>
       
		<div class="form-group row">
		<div class="col-sm-11" style="float: right;">	
				<button class="btn btn-lg btn-danger btn-block" name="batal">BATAL</button>
				<button class="btn btn-lg btn-primary btn-block" name="tambah">TAMBAH</button>
				<a href="<?php echo base_url("index.php/Layout/halaman_utama")?>" class="btn btn-lg btn-dark btn-block">KEMBALI</a>
		</div>	
		</div>
	</div>
</form>
		</div>

		
	</div>


	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src=" <?php echo base_url("tinputsurvey/vendor/jquery/jquery-3.2.1.min.js")?>"></script>
<!--===============================================================================================-->
	<script src=" <?php echo base_url("tinputsurvey/vendor/bootstrap/js/popper.js")?>"></script>
	<script src=" <?php echo base_url("tinputsurvey/vendor/bootstrap/js/bootstrap.min.js")?>"></script>
<!--===============================================================================================-->
	<script src=" <?php echo base_url("tinputsurvey/vendor/select2/select2.min.js")?>"></script>
<!--===============================================================================================-->
	<script src=" <?php echo base_url("tinputsurvey/js/main.js")?>"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src=" <?php echo base_url("https://www.googletagmanager.com/gtag/js?id=UA-23581568-13")?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>
</html>

	