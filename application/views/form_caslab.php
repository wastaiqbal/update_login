<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Data Alumni Unas</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url("vendor/fontawesome-free/css/all.min.css ")?>" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="<?php echo base_url("vendor/datatables/dataTables.bootstrap4.css ")?>" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href=" <?php echo base_url("css/sb-admin.css")?>" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="index.html">Lab FTKI</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="<?php echo base_url("#")?>">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
      <div class="input-group">
        <div class="input-group-append">
        <div style="margin-top: 50px;" color="white"><center>
        
        
        
        </center></div>
        </div>
      </div>
    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
      
    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url("index.php/Layout/halaman_utama")?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Home</span>
        </a>
      </li>
      
      
     

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-fw fa-table"></i>
          <span>Peserta</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Pilih Lab</h6>
          <a class="dropdown-item" href=" <?php echo base_url('index.php/Layout/view_caslab_ecom')  ?>">E-Commerce</a>
          <a class="dropdown-item" href=" <?php echo base_url('index.php/Layout/view_caslab_mcv')  ?>">MCV</a>
          <a class="dropdown-item" href=" <?php echo base_url('index.php/Layout/view_caslab_ndc')  ?>">NDC</a>
          <a class="dropdown-item" href=" <?php echo base_url('index.php/Layout/view_caslab_ai')  ?>">Artificial Intelegent</a>
          
        </div>
      </li>

    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
        <ol class="breadcrumb">
          <li class="breadcrumb-item">
            <a>
            <?php
            $alumni = "Formulir Pendaftaran Asisten Laboratorium";
            echo $alumni;
            
          ?>
            </a>
          </li>
          
        </ol>

        <!-- Icon Cards-->
        <div class="row">
        
        </div>

        <!-- Area Chart Example-->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-chart-area"></i>
            Input Data Recruitment</div>
          <div class="card-body">
            
			
<form action="<?php echo base_url('index.php/Recruitment/input_recruitment_caslab') ?>"  method="post" enctype="multipart/form-data">
	<div class="container">
		
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> NPM </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="npm" placeholder="NPM" required>
			</div>
		</div>
    <div class="form-group row">
			<label class="col-sm-2 col-form-label"> NAMA </label>
			<div class="col-sm-8">
				<input type="text" class="form-control" name="nama" placeholder="NAMA LENGKAP" required>
			</div>
		</div>
    <div class="form-group row">
			<label class="col-sm-2 col-form-label"> E-MAIL </label>
			<div class="col-sm-8">
				<input type="email" class="form-control" name="email" placeholder="MASUKAN E-MAIL" required>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">PRODI</label>
			<div class="form-group col-sm-8">	
					<input type="radio" name="prodi" value="TI" > TI <br>
                    <input type="radio" name="prodi" value="SI" > SI <br>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-2 col-form-label"> LABORATORIUM </label>
			<div class="col-sm-8">
                <select class="form-control" name="lab" required >
				    <option value="E-Commerce"> E-Commerce </option>
                    <option value="Multimedia and Computer Vision"> Multimedia and Computer Vision </option>
                    <option value="Network Data and Communication"> Network Data and Communication </option>
                    <option value="Artificial Intelegent"> Artificial Intelegent </option>
                </select>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label"> DESKRIPSIKAN SINGKAT KEMAMPUAN PROGRAMMING ANDA </label>
			<div class="form-group col-sm-8">	
                <textarea name="programming" class="form-control" cols="20" rows="4"></textarea>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label"> DESKRIPSIKAN SINGKAT KEMAMPUAN NETWORKING ANDA </label>
			<div class="form-group col-sm-8">	
                <textarea name="networking" class="form-control" cols="20" rows="4"></textarea>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label"> DESKRIPSIKAN SINGKAT KEMAMPUAN MULTIMEDIA ANDA </label>
			<div class="form-group col-sm-8">	
                <textarea name="mulmed" class="form-control" cols="20" rows="4"></textarea>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label"> ALASAN MENDAFTAR ASLAB </label>
			<div class="form-group col-sm-8">	
                <textarea name="alasan" class="form-control" cols="20" rows="4"></textarea>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label"> DESKRIPSIKAN SINGKAT TENTANG DIRI ANDA </label>
			<div class="form-group col-sm-8">	
                <textarea name="about" class="form-control" cols="20" rows="4"></textarea>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Upload cv MAX (2 MB DAN WAJIB PDF) </label>
			<div class="form-group col-sm-8">	
                    <input type="file" name="filefoto">
			</div>
		</div>
		<div class="form-group row">
		<div class="col-sm-10" style="float: right;">	
				<button class="btn btn-lg btn-danger" name="batal">BATAL</button>
				<button class="btn btn-lg btn-primary" name="tambah">TAMBAH</button>
		</div>	
		</div>
	</div>
</form>

          </div>
        </div>

          </div>
          <div class="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
        </div>

      

      <!-- Sticky Footer -->
      <footer class="sticky-footer">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Data Tracker Alumni Create by Danang 173112706450221</span>
          </div>
        </div>
      </footer>

    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href=" <?php echo base_url("#page-top")?>">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href=" <?php echo base_url("login.html")?>">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url("vendor/jquery/jquery.min.js ")?>"></script>
  <script src="<?php echo base_url("vendor/bootstrap/js/bootstrap.bundle.min.js ")?>"></script>

  <!-- Core plugin JavaScript-->
  <script src=" <?php echo base_url("vendor/jquery-easing/jquery.easing.min.js")?>"></script>

  <!-- Page level plugin JavaScript-->
  <script src=" <?php echo base_url("vendor/chart.js/Chart.min.js")?>"></script>
  <script src="<?php echo base_url("vendor/datatables/jquery.dataTables.js ")?>"></script>
  <script src=" <?php echo base_url("vendor/datatables/dataTables.bootstrap4.js")?>"></script>

  <!-- Custom scripts for all pages-->
  <script src=" <?php echo base_url("js/sb-admin.min.js")?>"></script>

  <!-- Demo scripts for this page-->
  <script src="<?php echo base_url("js/demo/datatables-demo.js ")?>"></script>
  <script src=" <?php echo base_url("js/demo/chart-area-demo.js")?>"></script>

</body>

</html>
