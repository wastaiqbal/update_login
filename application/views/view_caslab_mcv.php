<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Data Alumni Unas</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url("vendor/fontawesome-free/css/all.min.css ")?>" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="<?php echo base_url("vendor/datatables/dataTables.bootstrap4.css ")?>" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href=" <?php echo base_url("css/sb-admin.css")?>" rel="stylesheet">

</head>

<body id="page-top">

  <nav class="navbar navbar-expand navbar-dark bg-dark static-top">

    <a class="navbar-brand mr-1" href="index.html">Lab FTKI</a>

    <button class="btn btn-link btn-sm text-white order-1 order-sm-0" id="sidebarToggle" href="<?php echo base_url("#")?>">
      <i class="fas fa-bars"></i>
    </button>

    <!-- Navbar Search -->
    <form class="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0">
      <div class="input-group">
        <div class="input-group-append">
        <div style="margin-top: 50px;" color="white"><center>
        
        
        
        </center></div>
        </div>
      </div>
    </form>

    <!-- Navbar -->
    <ul class="navbar-nav ml-auto ml-md-0">
      
    </ul>

  </nav>

  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="sidebar navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url("index.php/Layout/halaman_utama")?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Home</span>
        </a>
      </li>
      
      
     

      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="pagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-fw fa-table"></i>
          <span>Peserta</span>
        </a>
        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
          <h6 class="dropdown-header">Pilih Lab</h6>
          <a class="dropdown-item" href=" <?php echo base_url('index.php/Layout/view_caslab_ecom')  ?>">E-Commerce</a>
          <a class="dropdown-item" href=" <?php echo base_url('index.php/Layout/view_caslab_mcv')  ?>">MCV</a>
          <a class="dropdown-item" href=" <?php echo base_url('index.php/Layout/view_caslab_ndc')  ?>">NDC</a>
          <a class="dropdown-item" href=" <?php echo base_url('index.php/Layout/view_caslab_ai')  ?>">Artificial Intelegent</a>
          <h6 class="dropdown-header">Foem Pendaftaran</h6>
          <a class="dropdown-item" href=" <?php echo base_url('index.php/Layout/caslab')  ?>">Folmulir</a>
        </div>
      </li>

    </ul>

    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
       
            </a>
          </li>
        </ol>

        <!-- Icon Cards-->
        <div class="row">
        
        </div>

      

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            <b> Berikut merupakan Dokumen data recruitment yang mendaftar 
            <?php
            $alumni = "calon aslab Multimedia and Computer Vision";
            echo $alumni;
            
          ?>
         </b>
         

            </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>Tahun</th>
                    <th>NPM</th>
                    <th>Nama</th>
                    <th>Prodi</th>
                    <th>Status</th>
                   
                  </tr>
                </thead>
                <tfoot>
                  
                </tfoot>
                <tbody>
                <?php
                  $alumni = $this->session->userdata('prodi');
                  $lab = "Multimedia and Computer Vision";
                  $tanggal=getdate();
                  $thn1 = $tanggal['year']+1;
                  $y= $tanggal['year'].'-'.$thn1;
                  $cek_query=$this->UserRecruitment->cek_data_caslab_mcv($lab); 

                  foreach ($cek_query->result_array() as $row)
                  {       
                ?>
                  <tr>
                    <td><?php echo $row['tahun'] ; ?></td>
                    <td><?php echo $row['npm'] ; ?></td>
                    <td><?php echo $row['nama'] ; ?></td>
                    <td><?php echo $row['prodi'] ; ?></td>
                    <td><?php echo $row['lolos'] ; ?></td>
                    
                  </tr>

                    <?php } ?>
                 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      
    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href=" <?php echo base_url("#page-top")?>">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href=" <?php echo base_url("login.html")?>">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url("vendor/jquery/jquery.min.js ")?>"></script>
  <script src="<?php echo base_url("vendor/bootstrap/js/bootstrap.bundle.min.js ")?>"></script>

  <!-- Core plugin JavaScript-->
  <script src=" <?php echo base_url("vendor/jquery-easing/jquery.easing.min.js")?>"></script>

  <!-- Page level plugin JavaScript-->
  <script src=" <?php echo base_url("vendor/chart.js/Chart.min.js")?>"></script>
  <script src="<?php echo base_url("vendor/datatables/jquery.dataTables.js ")?>"></script>
  <script src=" <?php echo base_url("vendor/datatables/dataTables.bootstrap4.js")?>"></script>

  <!-- Custom scripts for all pages-->
  <script src=" <?php echo base_url("js/sb-admin.min.js")?>"></script>

  <!-- Demo scripts for this page-->
  <script src="<?php echo base_url("js/demo/datatables-demo.js ")?>"></script>
  <script src=" <?php echo base_url("js/demo/chart-area-demo.js")?>"></script>

</body>

</html>
