<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Data Alumni Unas</title>

  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url("vendor/fontawesome-free/css/all.min.css ")?>" rel="stylesheet" type="text/css">

  <!-- Page level plugin CSS-->
  <link href="<?php echo base_url("vendor/datatables/dataTables.bootstrap4.css ")?>" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href=" <?php echo base_url("css/sb-admin.css")?>" rel="stylesheet">

</head>

<body id="page-top">


    <!-- Navbar Search -->
   

    <!-- Navbar -->
    

  <div id="wrapper">


    <div id="content-wrapper">

      <div class="container-fluid">

        <!-- Breadcrumbs-->
       

        <!-- Icon Cards-->
        <div class="row">
        
        </div>

      

        <!-- DataTables Example -->
        <div class="card mb-3">
          <div class="card-header">
            <i class="fas fa-table"></i>
            Berikut merupakan modul pada laboratorium FTKI
         
          
            
            </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>ID Modul</th>
                    <th>Modul</th>
                    <th>Mata Kuliah</th>
                    <th>Semester</th>
                    <th>Upload By</th>
                    <th>The laboratory that uploaded it</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tfoot>
                  
                </tfoot>
                <tbody>
                <?php
                
                  $cek_query=$this->UserModul->modul_semua_lab(); 
                


                  foreach ($cek_query->result_array() as $row)
                  {       
                ?>
                  <tr>
                    <td><?php echo $row['id_modul'] ; ?></td>
                    <td><?php echo $row['modul'] ; ?></td>
                    <td><?php echo $row['matkul'] ; ?></td>
                    <td><?php echo $row['semester'] ; ?></td>
                    <td><?php echo $row['upload_by'] ; ?></td>
                    <td><?php
                     $alumni = $row['laboratorium_upload'];
                     echo $alumni;    
                     ?></td>
                     <td>
                                     
                                                <a href="<?php echo base_url().'assets/images/'.$row['file'];?>" class="btn btn-outline-warning" >View doct</a>
                                               
                                                
                    </td>
                    
                  </tr>

                    <?php } ?>
                 
                </tbody>
              </table>
            </div>
          </div>
          
        </div>

      </div>
      <!-- /.container-fluid -->

      <!-- Sticky Footer -->
      
    </div>
    <!-- /.content-wrapper -->

  </div>
  <!-- /#wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href=" <?php echo base_url("#page-top")?>">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href=" <?php echo base_url("login.html")?>">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url("vendor/jquery/jquery.min.js ")?>"></script>
  <script src="<?php echo base_url("vendor/bootstrap/js/bootstrap.bundle.min.js ")?>"></script>

  <!-- Core plugin JavaScript-->
  <script src=" <?php echo base_url("vendor/jquery-easing/jquery.easing.min.js")?>"></script>

  <!-- Page level plugin JavaScript-->
  <script src=" <?php echo base_url("vendor/chart.js/Chart.min.js")?>"></script>
  <script src="<?php echo base_url("vendor/datatables/jquery.dataTables.js ")?>"></script>
  <script src=" <?php echo base_url("vendor/datatables/dataTables.bootstrap4.js")?>"></script>

  <!-- Custom scripts for all pages-->
  <script src=" <?php echo base_url("js/sb-admin.min.js")?>"></script>

  <!-- Demo scripts for this page-->
  <script src="<?php echo base_url("js/demo/datatables-demo.js ")?>"></script>
  <script src=" <?php echo base_url("js/demo/chart-area-demo.js")?>"></script>

</body>

</html>
