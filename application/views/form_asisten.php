<!-- FORM -->


<!DOCTYPE html>
<html lang="en">
<head>
	<title>Survey Pengguna Alumni</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href=" <?php echo base_url("tinputsurvey/images/icons/favicon.ico")?>"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" <?php echo base_url("tinputsurvey/vendor/bootstrap/css/bootstrap.min.css")?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href=" <?php echo base_url("tinputsurvey/fonts/font-awesome-4.7.0/css/font-awesome.min.css")?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("tinputsurvey/fonts/Linearicons-Free-v1.0.0/icon-font.min.css")?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("tinputsurvey/vendor/animate/animate.css")?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("tinputsurvey/vendor/css-hamburgers/hamburgers.min.css")?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("tinputsurvey/vendor/select2/select2.min.css")?>">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("tinputsurvey/css/util.css")?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url("tinputsurvey/css/main.css")?>">
<!--===============================================================================================-->
</head>
<body>



<div class="container-contact100" style="background-image:  url('<?php echo base_url("tinputsurvey/images1/bg-01.jpg")?>');">
		<div class="wrap-contact100">
	<form action="<?php echo base_url('index.php/Penghubung/proses_daftar') ?>" method="post" enctype="multipart/form-data" autocomplete="off">
	<div class="container">
        
        <div class="form-group row">  
            <label>FORM DAFTAR AKSES WEBSITE LABORATPRIUM</label>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Nomor Pokok Mahasiswa (NPM)</label>
			<div class="form-group col-sm-8">	
				<input type="text" name="npm" class="form-control" required >
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Password</label>
			<div class="form-group col-sm-8">	
				<input type="text" name="password" class="form-control" required >
			</div>
        </div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Nama</label>
			<div class="form-group col-sm-8">	
				<input type="text" name="nama_aslab" class="form-control" required >
			</div>
		</div>
		<div class="form-group row">
			<label  class="col-sm-3 col-form-label">Prodi</label>
			<div class="form-group col-sm-8">	
					<input type="radio" name="prodi" value="TI" > TI <br>
                    <input type="radio" name="prodi" value="SI" > SI <br>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label"> Laboratorium </label>
			<div class="col-sm-8">
                <select class="form-control" name="laboratorium" required >
                    <option value="E-Commerce"> E-Commerce </option>
                    <option value="Multimedia and Computer Vision"> Multimedia and Computer Vision </option>
                    <option value="Network Data and Communication"> Network Data and Communication </option>
                    <option value="Artificial Intelegent"> Artificial Intelegent </option>
                </select>
			</div>
		</div>
        <div class="form-group row">
            <label  class="col-sm-3 col-form-label">Jabatan</label>
			<div class="form-group col-sm-8">	   
            <select class="form-control" name="jabatan">
                    <option value="1"> KEPALA LABORATORIM </option>
                    <option value="2"> kOORDINATOR LABORATORIUM </option>
                    <option value="3"> WAKIL kOORDINATOR LABORATORIUM </option>
                    <option value="4"> SEKRETARIS LABORATORIUM </option>
                    <option value="5"> PENJADWALAN LABORATORIUM </option> 
                    <option value="6"> DIVISI HUMAS LABORATORIUM </option>
                    <option value="7"> DIVISI PENGEMBANGAN LABORATORIUM </option>       
                </select>
			</div>
		</div>
        <div class="form-group row">
			<label class="col-sm-3 col-form-label"> Tahun Masuk </label>
			<div class="col-sm-8">
                <select class="form-control" name="tahun_masuk" required >
                    <?php 
                         $tahun=getdate();
                         $y = $tahun['year']; 
                         
                        for ($n=1949; $n <= $y ; $n++) { ?>
                            <option value="<?php echo $n; ?>" > <?php echo $n; ?> </option>
                    <?php } ?>
                </select>
			</div>
		</div>
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">Upload Foto</label>
			<div class="form-group col-sm-8">	
                    <input type="file" name="filefoto">
			</div>
		</div>
      
        <div class="form-group row">
			<label  class="col-sm-3 col-form-label">About</label>
			<div class="form-group col-sm-8">	
                <textarea name="about" class="form-control" cols="40" rows="4"></textarea>
			</div>
		</div>
       
		<div class="form-group row">
		<div class="col-sm-11" style="float: right;">	
				<button class="btn btn-lg btn-danger btn-block" name="batal">BATAL</button>
				<button class="btn btn-lg btn-primary btn-block" name="tambah">TAMBAH</button>
				<a href="<?php echo base_url("index.php/Layout/halaman_utama")?>" class="btn btn-lg btn-dark btn-block">KEMBALI</a>
		</div>	
		</div>
	</div>
</form>
		</div>

		
	</div>


	<div id="dropDownSelect1"></div>

<!--===============================================================================================-->
	<script src=" <?php echo base_url("tinputsurvey/vendor/jquery/jquery-3.2.1.min.js")?>"></script>
<!--===============================================================================================-->
	<script src=" <?php echo base_url("tinputsurvey/vendor/bootstrap/js/popper.js")?>"></script>
	<script src=" <?php echo base_url("tinputsurvey/vendor/bootstrap/js/bootstrap.min.js")?>"></script>
<!--===============================================================================================-->
	<script src=" <?php echo base_url("tinputsurvey/vendor/select2/select2.min.js")?>"></script>
<!--===============================================================================================-->
	<script src=" <?php echo base_url("tinputsurvey/js/main.js")?>"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src=" <?php echo base_url("https://www.googletagmanager.com/gtag/js?id=UA-23581568-13")?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>

</body>
</html>

	