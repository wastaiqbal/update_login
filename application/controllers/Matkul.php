<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Matkul extends CI_Controller {

    public function __construct(){
        parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('Dataalumni');
		$this->load->model('UserModul');
		$this->load->model('UserMatkul');
		$this->load->model('UserSuratMasuk');
		$this->load->model('UserSuratKeluar');
		$this->load->model('UserPenjadwalan');
		$this->load->model('UserRecruitment');
		$this->load->model('UserArsip');
		$this->load->model('UserAkun');
		$this->load->library('session');
		$this->load->library('upload');
		
    }

	
	function input_matkul(){
		$kode_matkul = $this->input->post('kode_matkul');
        $matkul = $this->input->post('matkul');
		$lab = $this->input->post('lab');
		$hari = $this->input->post('hari');
		$jam = $this->input->post('jam');
		$kelas = $this->input->post('kelas');
		$dosen = $this->input->post('dosen');
		$tahun_ajaran = $this->input->post('tahun_ajaran');
        $this->load->library('ciqrcode'); //pemanggilan library QR CODE
 
        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/images/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);
		$w = $kode_matkul . "-" . $matkul . "-" . $hari . "-" . $jam . "-" . $kelas . "-" .  $dosen;
        $image_name=$w.'.png'; //buat name dari qr code sesuai dengan nim
 
        $params['data'] = $w; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
		$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
		
		$this->UserMatkul->input_matkul($kode_matkul, $lab, $kelas, $matkul, $hari, $jam, $dosen, $image_name, $tahun_ajaran);
		return $this->load->view('daftar_matkul.php');
	}

	function ubah_matkul(){
		$kode_matkul = $this->input->post('kode_matkul');
        $matkul = $this->input->post('matkul');
		$lab = $this->input->post('lab');
		$hari = $this->input->post('hari');
		$jam = $this->input->post('jam');
		$kelas = $this->input->post('kelas');
		$dosen = $this->input->post('dosen');
		$tahun_ajaran = $this->input->post('tahun_ajaran');
        $this->load->library('ciqrcode'); //pemanggilan library QR CODE
 
        $config['cacheable']    = true; //boolean, the default is true
        $config['cachedir']     = './assets/'; //string, the default is application/cache/
        $config['errorlog']     = './assets/'; //string, the default is application/logs/
        $config['imagedir']     = './assets/images/'; //direktori penyimpanan qr code
        $config['quality']      = true; //boolean, the default is true
        $config['size']         = '1024'; //interger, the default is 1024
        $config['black']        = array(224,255,255); // array, default is array(255,255,255)
        $config['white']        = array(70,130,180); // array, default is array(0,0,0)
        $this->ciqrcode->initialize($config);
		$w = $kode_matkul . "-" . $matkul . "-" . $hari . "-" . $jam . "-" . $kelas . "-" .  $dosen;
        $image_name=$w.'.png'; //buat name dari qr code sesuai dengan nim
 
        $params['data'] = $w; //data yang akan di jadikan QR CODE
        $params['level'] = 'H'; //H=High
        $params['size'] = 10;
        $params['savename'] = FCPATH.$config['imagedir'].$image_name; //simpan image QR CODE ke folder assets/images/
		$this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
		
		$this->UserMatkul->ubah_matkul($kode_matkul, $lab, $kelas, $matkul, $hari, $jam, $dosen, $image_name, $tahun_ajaran);
		return $this->load->view('daftar_matkul.php');
	}

	function input_berita_acara(){
		$kode_matkul = $this->input->post('kode_matkul');
		date_default_timezone_set('Asia/Jakarta');  
   		$lu =  date("l, j F Y, H:i");
		$value = $this->input->post('nama');
		
		$this->UserMatkul->input_berita_acara($value, $lu, $kode_matkul);
		return $this->load->view('menu.php');
	}

	function hapus(){
		$ni = $this->input->get('ni');
	
		$cek_query_insert=$this->UserMatkul->hapus_matkul($ni);
		return $this->load->view('daftar_matkul.php');
	}

    public function proses_daftar(){
		$config['upload_path'] = './assets/images/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 5000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){

	        if ($this->upload->do_upload('filefoto')){ //nama fiel yg ada diview
	            $gbr = $this->upload->data();
	            //Compress Image
	            $config['image_library']='gd2';
	            $config['source_image']='./assets/images/'.$gbr['file_name'];
	            $config['new_image']= './assets/images/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
	            $this->image_lib->resize();

	            $gambar = $gbr['file_name'];
				$nama_aslab = $this->input->post('nama_aslab');
				$fakultas = $this->input->post('fakultas');
				$npm = $this->input->post('npm');
				$prodi = $this->input->post('prodi');
				$laboratorium = $this->input->post('laboratorium');
				$jabatan = $this->input->post('jabatan');
				$tahun_masuk = $this->input->post('tahun');
				$about = $this->input->post('about');
				
				$password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);
				
				$cek_nid = $this->UserModel->cek_data_nid($npm);

				$cek_data_nid =  $cek_nid->num_rows();
				if($cek_data_nid > 0){
					foreach($cek_nid->result_array() as $row){
						echo "<script>alert('NID ".$row['npm']." sudah digunakan ".$row['nama']."')</script>";
					}
					return $this->load->view('form_asisten.php');
				}
				else{
					$this->UserModel->aslab($npm,$password,$nama_aslab,$prodi,$laboratorium,$jabatan,$tahun_masuk,$about,$gambar);
					echo "<script>alert('Berhasil mengupload data')</script>";
					return $this->load->view('form_asisten.php');
				}			
			}               
		}
		
        else{
			echo "<script>alert('UPLOAD KOSONG')</script>";
					return $this->load->view('form_asisten.php');;
		}
				
	}

	

	function login(){
		$username = $this->input->post('username');
	    $password = $this->input->post('password');
	        $cek_query_insert=$this->UserModel->login_user($username);     
			$data=$cek_query_insert->row_array();
			
			$a = $this->UserModel->cek_data_npm_aslab();
			$ab = $a->num_rows();

			foreach($a->result_array() as $row){			
				if(($row['Laboratorium'] = $data['Laboratorium'])){
					if(password_verify($password, $data['password'])){
						$this->session->set_userdata('npm', $data['npm']);
						$this->session->set_userdata('password', $data['password']);
						$this->session->set_userdata('jabatan', $data['Jabatan']);
						$this->session->set_userdata('nama', $data['nama']);
						$this->session->set_userdata('lab', $data['Laboratorium']);
						$this->session->set_userdata('foto', $data['foto']);
		
						
						return $this->load->view('menu.php');
							  
						$this->input->set_cookie('npm', $username, 450);
						$this->input->set_cookie('Nama', $data['nama'], 450);      
					}        
					else{  
						echo "<script>alert('Password yang anda masukan salah, pastikan lagi. Jika anda lupa dengan password harap hubungi BPSI unas')</script>";
						return $this->load->view('login.php');
					}
				}
				else{
					echo "<script>alert('Anda belum terdaftar, silahkan hubungi BPSI unas')</script>";
						return $this->load->view('login.php');
				}
			
			}
			

			

	               
	}
        

	function utama()
	{
		$this->load->view('main.php');
	}
	
	function survey_alumni(){
		$nim = $this->input->post('nim');
		$nama = $this->input->post('nama');
		$prodi = $this->input->post('prodi');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$tlp = $this->input->post('tlp');
		$stbekerja = $this->input->post('pekerjaan');
		$thmasuk = $this->input->post('tahun_masuk');
		$thlulus = $this->input->post('tahun_lulus');
		$ipk = $this->input->post('ipk');
		$nama_perusahaan = $this->input->post('nama_perusahaan');
		$alamat_perusahaan = $this->input->post('alamat_perusahaan');
		$tahun_bekerja = $this->input->post('tahun_bekerja');
		$sallery = $this->input->post('sallery');
		$jabatan = $this->input->post('jabatan_perusahaan');
		$idm2 = $this->input->post('idm2');
		$idm1 = $this->input->post('idm1');
		$alasan_belum_bekerja = $this->input->post('alasan_belum_bekerja');
		$profesi = $this->input->post('profesi');
		$masukan = $this->input->post('masukan');
		
		
		$cek_nim = $this->Dataalumni->cek_data_alumni($nim);
		$cek_data_nim =  $cek_nim->num_rows();

		if($cek_data_nim > 0){
			foreach($cek_nim->result_array() as $row){
				echo "<script>alert('NPM ".$row['nim']." sudah digunakan ".$row['nama_alumni']."')</script>";
			}
			return $this->load->view('survey_alumni.php');
		}
		else{
			$this->Dataalumni->survey_alumni($nim, $nama,$prodi,$jenis_kelamin,$tlp,
			$stbekerja, $thmasuk, $thlulus, $ipk, $nama_perusahaan, $alamat_perusahaan, 
			$tahun_bekerja, $sallery, $jabatan, $idm1, $idm2, $alasan_belum_bekerja, 
			$profesi, $masukan);
			return $this->load->view('main.php');
		}			
	}

	function survey_pengguna_alumni(){
		$nama = $this->input->post('nama');
		$nama_perusahaan = $this->input->post('nama_perusahaan');
		$alamat_perusahaan = $this->input->post('alamat_perusahaan');
		$tahun_bekerja = $this->input->post('tahun_bekerja');
		$idm1 = $this->input->post('idm1');
		$idm2 = $this->input->post('idm2');
		$profesi = $this->input->post('profesi');
		$language = $this->input->post('language');
		$pti = $this->input->post('pti');
		$komunikasi1 = $this->input->post('komunikasi1');
		$komunikasi2 = $this->input->post('komunikasi2');
		$tw = $this->input->post('tw');
		$pd1 = $this->input->post('pd1');
		$pd2 = $this->input->post('pd2');
		$pd3 = $this->input->post('pd3');
		$pd4 = $this->input->post('pd4');
		$pd5 = $this->input->post('pd5');
		$pd6 = $this->input->post('pd6');
		$kepuasan = $this->input->post('kepuasan');
		$masukan = $this->input->post('masukan');

		$this->Dataalumni->survey_pengguna_alumni($nama,$nama_perusahaan,$alamat_perusahaan,$tahun_bekerja,
		$idm1, $idm2, $profesi, $language, $pti, $komunikasi1, $komunikasi2, $tw, $pd1, $pd2, $pd3, $pd4, $pd5, $pd6,
		$kepuasan, $masukan);
		return $this->load->view('main.php');
	}
	

	
}
