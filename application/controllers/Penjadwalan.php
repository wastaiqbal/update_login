<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Penjadwalan extends CI_Controller {

    public function __construct(){
        parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('Dataalumni');
		$this->load->model('UserModul');
		$this->load->model('UserMatkul');
		$this->load->model('UserSuratMasuk');
		$this->load->model('UserSuratKeluar');
        $this->load->model('UserPenjadwalan');
        $this->load->model('UserRecruitment');
        $this->load->model('UserArsip');
        $this->load->model('UserAkun');
		$this->load->library('session');
		$this->load->library('upload');
		
    }

    function hapus(){
		$ni = $this->input->get('ni');
	
		$cek_query_insert=$this->UserPenjadwalan->hapus_surat_penjadwalan($ni);
		return $this->load->view('penjadwalan.php');
	}

    public function input_surat_penjadwalan(){
		$config['upload_path'] = './assets/images/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){

	        if ($this->upload->do_upload('filefoto')){ //nama fiel yg ada diview
	            $gbr = $this->upload->data();
	            //Compress Image
	            $config['image_library']='gd2';
	            $config['source_image']='./assets/images/'.$gbr['file_name'];
	            $config['new_image']= './assets/images/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
	            $this->image_lib->resize();

	            $gambar = $gbr['file_name'];
				$nomer = $this->input->post('id_dokumen');
				$bulan = $this->input->post('bulan');
                $tahun = $this->input->post('tahun');
                $keterangan = $this->input->post('ket');
                $laboratorium = $this->input->post('lab');
				
			
				$cek_nid = $this->UserPenjadwalan->cek_data_surat_penjadwalan($nomer);

				$cek_data_nid =  $cek_nid->num_rows();
				if($cek_data_nid > 0){
					foreach($cek_nid->result_array() as $row){
						echo "<script>alert('Nomer ".$row['id_dokumen']." sudah digunakan untuk ".$row['bulan']."')</script>";
					}
					return $this->load->view('penjadwalan.php');
				}
				else{
					$this->UserPenjadwalan->input_surat_penjadwalan($nomer,$bulan,$tahun,$gambar,$laboratorium,$keterangan,$laboratorium);
					echo "<script>alert('Berhasil mengupload data')</script>";
					return $this->load->view('penjadwalan.php');
				}			
			}               
		}
		
        else{
			echo "<script>alert('Tidak ada file yang diupload')</script>";
					return $this->load->view('penjadwalan.php');;
		}
				
    }
    
    public function edit_surat_penjadwalan(){
		$config['upload_path'] = './assets/images/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){

	        if ($this->upload->do_upload('filefoto')){ //nama fiel yg ada diview
	            $gbr = $this->upload->data();
	            //Compress Image
	            $config['image_library']='gd2';
	            $config['source_image']='./assets/images/'.$gbr['file_name'];
	            $config['new_image']= './assets/images/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
	            $this->image_lib->resize();

	            $gambar = $gbr['file_name'];
				$nomer = $this->input->post('id_dokumen');
				$bulan = $this->input->post('bulan');
                $tahun = $this->input->post('tahun');
                $keterangan = $this->input->post('ket');
                $laboratorium = $this->input->post('labo');
				
					$this->UserPenjadwalan->edit_surat_penjadwalan($nomer,$bulan,$tahun,$gambar,$laboratorium,$keterangan,$laboratorium);
					echo "<script>alert('Berhasil mengupload data')</script>";
					return $this->load->view('penjadwalan.php');
							
			}
			
			
		}
		
        else{
            $nomer = $this->input->post('id_dokumen');
            $bulan = $this->input->post('bulan');
            $tahun = $this->input->post('tahun');
            $keterangan = $this->input->post('ket');
            $laboratorium = $this->input->post('lab');

				
                $this->UserPenjadwalan->edit_surat_kosong_penjadwalan($nomer,$bulan,$tahun,$keterangan,$laboratorium);
                echo "<script>alert('Berhasil mengupload data')</script>";
                return $this->load->view('penjadwalan.php');
				
		}
				
	}
	
}
