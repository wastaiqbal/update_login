<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aksialumni extends CI_Controller {

	public function __construct(){
        parent::__construct();
		$this->load->model('Dataalumni');
		$this->load->model('UserModel');
		$this->load->library('session');
		$this->load->library('upload');
	}
	
	public function dta_delete(){
		$ni = $this->input->get('ni');
	
		$cek_query_insert=$this->Dataalumni->hapus_alumni($ni);
		return $this->load->view('alldata.php');
	}
	
	function dta_edit(){
		
		$nim = $this->input->post('nim');
		$nama = $this->input->post('nama');
		$prodi = $this->input->post('prodi');
		$jenis_kelamin = $this->input->post('jenis_kelamin');
		$tlp = $this->input->post('tlp');
		$stbekerja = $this->input->post('pekerjaan');
		$thmasuk = $this->input->post('tahun_masuk');
		$thlulus = $this->input->post('tahun_lulus');
		$ipk = $this->input->post('ipk');
		$nama_perusahaan = $this->input->post('nama_perusahaan');
		$alamat_perusahaan = $this->input->post('alamat_perusahaan');
		$tahun_bekerja = $this->input->post('tahun_bekerja');
		$sallery = $this->input->post('sallery');
		$jabatan = $this->input->post('jabatan_perusahaan');
		$idm2 = $this->input->post('idm2');
		$idm1 = $this->input->post('idm1');
		$alasan_belum_bekerja = $this->input->post('alasan_belum_bekerja');
		$profesi = $this->input->post('profesi');
		$masukan = $this->input->post('masukan');
		
		$this->Dataalumni->edit_data_survey_alumni($nim, $nama,$prodi,$jenis_kelamin,$tlp,
		$stbekerja, $thmasuk, $thlulus, $ipk, $nama_perusahaan, $alamat_perusahaan, 
		$tahun_bekerja, $sallery, $jabatan, $idm1, $idm2, $alasan_belum_bekerja, 
		$profesi, $masukan);
		return $this->load->view('alldata.php');
	}


}
