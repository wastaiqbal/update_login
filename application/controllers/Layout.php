<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Layout extends CI_Controller {

	public function __construct(){
        parent::__construct();
        $this->load->model('UserModel');
        $this->load->model('UserModul');
        $this->load->model('Dataalumni');
        $this->load->model('UserMatkul');
        $this->load->model('UserSuratMasuk');
        $this->load->model('UserSuratKeluar');
        $this->load->model('UserPenjadwalan');
        $this->load->model('UserRecruitment');
        $this->load->model('UserArsip');
        $this->load->model('UserAkun');
		$this->load->library('session');
		$this->load->library('upload');
	}
	
	

	function halaman_utama(){
		$this->load->view('main.php');
    }


	function view_all_data()
	{
		$x['data']=$this->UserModel->dta();
        $this->load->view('alldata.php', $x);
    }

    function stcakholder()
	{
        $this->load->view('alldata_perusahaan.php');
    }

    

    function track_study()
	{
        $this->load->view('alldata.php');
    }
    
    public function form_asisten(){
        $this->load->view('form_asisten.php');
	}
	
	public function form_pengguna_alumni(){
        $this->load->view('survey_alumni.php');
	}
	
	public function caslab(){
        $this->load->view('form_caslab.php');
    }

    public function try_caslab(){
        $year = date("Y");
        echo "<script>alert('Pendaftaran dibuka 15 November $year s/d 15 Desember $year ')</script>";
        $this->load->view('main.php');
    }

    public function email_caslab(){
        $this->load->view('email_caslab.php');
    }

    public function view_caslab(){
        $this->load->view('view_caslab.php');
    }
    
    public function view_caslab_ecom(){
        $this->load->view('view_caslab_ecom.php');
    }
    public function view_caslab_ndc(){
        $this->load->view('view_caslab_ndc.php');
    }
    public function view_caslab_mcv(){
        $this->load->view('view_caslab_mcv.php');
    }
    public function view_caslab_ai(){
        $this->load->view('view_caslab_ai.php');
    }

    

    public function form_login(){
        $this->load->view('login.php');
    }

    public function view(){
		//$data['user'] = $this->UserModel->data_alumni();
		$this->load->view('alldata.php');
	}

    function menu(){
		
        $this->load->view('menu.php');
    }

    function penjadwalan(){
		
        $this->load->view('penjadwalan.php');
    }


    function vedit_data_survey_alumni(){
		
        $this->load->view('edit_data_survey_alumni.php');
    }

   
    
    function modul()
	{
        $this->load->view('modul_lab.php');
    }

    function modul_lab()
	{
        $this->load->view('modul_lab.php');
    }

    function daftar_matkul()
	{
        $this->load->view('daftar_matkul.php');
    }

    function input_matkul()
	{
        $this->load->view('input_matkul.php');
    }
    
    function scan_matkul()
	{
        $this->load->view('scan_matkul.php');
    }

    function ubah_matkul()
	{
        $this->load->view('ubah_matkul.php');
    }

    function view_qr()
	{
        $this->load->view('barcode_matkul.php');
    }

    function input_modul()
	{
        $this->load->view('input_modul.php');
    }

    function view_modul()
	{
        $this->load->view('view_modul.php');
    }

    function edit_modul()
	{
        $this->load->view('ubah_modul.php');
    }

    function surat_masuk()
	{
        $this->load->view('surat_masuk.php');
    }

    function input_suratmasuk()
	{
        $this->load->view('input_suratmasuk.php');
    }

    function input_suratkeluar()
	{
        $this->load->view('input_suratkeluar.php');
    }
    
    function edit_surat_masuk()
	{
        $this->load->view('edit_surat_masuk.php');
    }

    function edit_surat_keluar()
	{
        $this->load->view('edit_surat_keluar.php');
    }

    function surat_keluar()
	{
        $this->load->view('surat_keluar.php');
    }

    function recruitment()
	{
        $this->load->view('recruitment.php');
    }

    function input_penjadwalan()
	{
        $this->load->view('input_penjadwalan.php');
    }

    function edit_surat_penjadwalan()
	{
        $this->load->view('edit_surat_penjadwalan.php');
    }

    function edit_recruitment()
	{
        $this->load->view('edit_recruitment.php');
    }

    function view_recruitment()
	{
        $this->load->view('view_recruitment.php');
    }

    function input_recruitment()
	{
        $this->load->view('input_recruitment.php');
    }

    function arsip_surat()
	{
        $this->load->view('arsip_surat.php');
    }

    function input_arsip_surat()
	{
        $this->load->view('input_arsip_surat.php');
    }

    function edit_arsip_surat()
	{
        $this->load->view('edit_arsip_surat.php');
    }

    function struktur_organisasi()
	{
        $this->load->view('struktur_organisasi.php');
    }

    function edit_akun()
	{
        $this->load->view('edit_akun.php');
    }

    function visitor_modul()
	{
        $this->load->view('visitor_modul.php');
    }
}
?>