<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Modul extends CI_Controller {

    public function __construct(){
        parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('Dataalumni');
		$this->load->model('UserModul');
		$this->load->model('UserMatkul');
		$this->load->model('UserSuratMasuk');
		$this->load->model('UserSuratKeluar');
		$this->load->model('UserPenjadwalan');
		$this->load->model('UserRecruitment');
		$this->load->model('UserAkun');
		$this->load->model('UserArsip');
		$this->load->library('session');
		$this->load->library('upload');
		
	}
	
	function hapus(){
		$ni = $this->input->get('ni');
	
		$cek_query_insert=$this->UserModul->hapus_modul($ni);
		return $this->load->view('modul_lab.php');
	}

    public function input_modul(){
		$config['upload_path'] = './assets/images/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){

	        if ($this->upload->do_upload('filefoto')){ //nama fiel yg ada diview
	            $gbr = $this->upload->data();
	            //Compress Image
	            $config['image_library']='gd2';
	            $config['source_image']='./assets/images/'.$gbr['file_name'];
	            $config['new_image']= './assets/images/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
	            $this->image_lib->resize();

	            $gambar = $gbr['file_name'];
				$id_modul = $this->input->post('id_modul');
				$modul = $this->input->post('modul');
				$matkul = $this->input->post('matkul');
                $semester = $this->input->post('semester');
                $upload_by = $this->input->post('upload_by');
				$laboratorium = $this->input->post('laboratorium_upload');
				
			
				$cek_nid = $this->UserModul->cek_data_modul($id_modul);

				$cek_data_nid =  $cek_nid->num_rows();
				if($cek_data_nid > 0){
					foreach($cek_nid->result_array() as $row){
						echo "<script>alert('ID Modul ".$row['id_modul']." sudah digunakan ".$row['modul']."')</script>";
					}
					return $this->load->view('modul_lab.php');
				}
				else{
					$this->UserModul->input_modul($id_modul,$modul,$matkul,$semester,$upload_by,$laboratorium,$gambar);
					echo "<script>alert('Berhasil mengupload data')</script>";
					return $this->load->view('modul_lab.php');
				}			
			}               
		}
		
        else{
			echo "<script>alert('UPLOAD KOSONG')</script>";
					return $this->load->view('form_asisten.php');;
		}
				
    }
    
    public function edit_modul(){
		$config['upload_path'] = './assets/images/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){

	        if ($this->upload->do_upload('filefoto')){ //nama fiel yg ada diview
	            $gbr = $this->upload->data();
	            //Compress Image
	            $config['image_library']='gd2';
	            $config['source_image']='./assets/images/'.$gbr['file_name'];
	            $config['new_image']= './assets/images/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
	            $this->image_lib->resize();

	            $gambar = $gbr['file_name'];
				$id_modul = $this->input->post('id_modul');
				$modul = $this->input->post('modul');
				$matkul = $this->input->post('matkul');
                $semester = $this->input->post('semester');
                $upload_by = $this->input->post('upload_by');
				$laboratorium = $this->input->post('laboratorium_upload');
				
			
				
					$this->UserModul->edit_modul($id_modul,$modul,$matkul,$semester,$upload_by,$laboratorium,$gambar);
					echo "<script>alert('Berhasil mengupload data')</script>";
					return $this->load->view('modul_lab.php');
							
			}
			
			
		}
		
        else{
				$id_modul = $this->input->post('id_modul');
				$modul = $this->input->post('modul');
				$matkul = $this->input->post('matkul');
                $semester = $this->input->post('semester');
                $upload_by = $this->input->post('upload_by');
				$laboratorium = $this->input->post('laboratorium_upload');
				
			
				
					$this->UserModul->edit_modul_kosong($id_modul,$modul,$matkul,$semester,$upload_by,$laboratorium);
					echo "<script>alert('Berhasil mengupload data')</script>";
					return $this->load->view('modul_lab.php');
				
		}
				
	}
	
}
