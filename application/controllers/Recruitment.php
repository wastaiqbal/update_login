<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recruitment extends CI_Controller {

    public function __construct(){
        parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('Dataalumni');
		$this->load->model('UserModul');
		$this->load->model('UserMatkul');
		$this->load->model('UserSuratMasuk');
		$this->load->model('UserSuratKeluar');
		$this->load->model('UserPenjadwalan');
		$this->load->model('UserRecruitment');
		$this->load->model('UserArsip');
		$this->load->model('UserAkun');
		$this->load->library('session');
		$this->load->library('upload');
		
    }

    public function input_recruitment(){
		$config['upload_path'] = './assets/images/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){

	        if ($this->upload->do_upload('filefoto')){ //nama fiel yg ada diview
	            $gbr = $this->upload->data();
	            //Compress Image
	            $config['image_library']='gd2';
	            $config['source_image']='./assets/images/'.$gbr['file_name'];
	            $config['new_image']= './assets/images/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
	            $this->image_lib->resize();

	            $gambar = $gbr['file_name'];
				$npm = $this->input->post('npm');
				$nama = $this->input->post('nama');
				$prodi = $this->input->post('prodi');
                $laboratorium = $this->input->post('lab');
                $programming = $this->input->post('programming');
				$networking = $this->input->post('networking');
				$mulmed = $this->input->post('mulmed');
				$alasan = $this->input->post('alasan');
				$about = $this->input->post('about');
				$tahun = $this->input->post('tahun');
				$lolos = "-";
				$tanggal=getdate();
                $tahun = $tanggal['year'];
				
			
				$cek_nid = $this->UserRecruitment->cek_data_surat_recruitment($npm, $laboratorium);
				
				$cek_data_nid =  $cek_nid->num_rows();
				if($cek_data_nid > 0){
					foreach($cek_nid->result_array() as $row){
						echo "<script>alert('NPM ".$row['npm']." sudah didaftarkan ')</script>";
					}
					return $this->load->view('recruitment.php');
				}
				else{
					$this->UserRecruitment->input_surat_recruitment($npm, $laboratorium, $nama, $email, $prodi, $programming, $mulmed, $networking, $gambar, $alasan, $about, $lolos, $tahun);
					echo "<script>alert('Berhasil mengupload data')</script>";
					return $this->load->view('recruitment.php');
				}			
			}               
		}
		
        else{
			echo "<script>alert('UPLOAD KOSONG')</script>";
					return $this->load->view('form_asisten.php');;
		}
				
	}
	
	public function input_recruitment_caslab(){
		$config['upload_path'] = './assets/images/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){

	        if ($this->upload->do_upload('filefoto')){ //nama fiel yg ada diview
	            $gbr = $this->upload->data();
	            //Compress Image
	            $config['image_library']='gd2';
	            $config['source_image']='./assets/images/'.$gbr['file_name'];
	            $config['new_image']= './assets/images/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
	            $this->image_lib->resize();

	            $gambar = $gbr['file_name'];
				$npm = $this->input->post('npm');
				$nama = $this->input->post('nama');
				$email = $this->input->post('email');
				$prodi = $this->input->post('prodi');
                $laboratorium = $this->input->post('lab');
                $programming = $this->input->post('programming');
				$networking = $this->input->post('networking');
				$mulmed = $this->input->post('mulmed');
				$alasan = $this->input->post('alasan');
				$about = $this->input->post('about');
				$tahun = $this->input->post('tahun');
				$lolos = "-";
				$tanggal=getdate();
                $tahun = $tanggal['year'];
				
			
				$cek_nid = $this->UserRecruitment->cek_data_surat_recruitment($npm, $laboratorium);
				
				$cek_data_nid =  $cek_nid->num_rows();
				if($cek_data_nid > 0){
					foreach($cek_nid->result_array() as $row){
						echo "<script>alert('NPM ".$row['npm']." sudah didaftarkan ')</script>";
					}
					return $this->load->view('view_recruitment.php');
				}
				else{
					$this->UserRecruitment->input_surat_recruitment($npm, $laboratorium, $nama, $email, $prodi, $programming, $mulmed, $networking, $gambar, $alasan, $about, $lolos, $tahun);
					echo "<script>alert('Berhasil mengupload data')</script>";
					return $this->load->view('view_recruitment.php');
				}			
			}               
		}
		
        else{
			echo "<script>alert('UPLOAD KOSONG')</script>";
					return $this->load->view('form_asisten.php');;
		}
				
    }

    
    public function edit_recruitment(){
		$config['upload_path'] = './assets/images/'; //path folder
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|xls|xlsx|ppt|ppt|zip|rar'; //type yang dapat diakses bisa anda sesuaikan
	    $config['encrypt_name'] = TRUE; //Enkripsi nama yang terupload
		$config['max_size']             = 7000;
		$config['max_width']            = 0;
		$config['max_height']           = 0;
	    $this->upload->initialize($config);
	    if(!empty($_FILES['filefoto']['name'])){

	        if ($this->upload->do_upload('filefoto')){ //nama fiel yg ada diview
	            $gbr = $this->upload->data();
	            //Compress Image
	            $config['image_library']='gd2';
	            $config['source_image']='./assets/images/'.$gbr['file_name'];
	            $config['new_image']= './assets/images/'.$gbr['file_name'];
	            $this->load->library('image_lib', $config);
	            $this->image_lib->resize();

	            $gambar = $gbr['file_name'];
				$npm = $this->input->post('npm');
				$nama = $this->input->post('nama');
				$email = $this->input->post('email');
				$prodi = $this->input->post('prodi');
                $laboratorium = $this->input->post('lab');
                $programming = $this->input->post('programming');
				$networking = $this->input->post('networking');
				$mulmed = $this->input->post('mulmed');
				$alasan = $this->input->post('alasan');
				$about = $this->input->post('about');
				$tahun = $this->input->post('tahun');
				$lolos = $this->input->post('status');
				$tanggal=getdate();
                $tahun = $tanggal['year'];
				
			
				
					$this->input_surat_recruitment->edit_surat_recruitment($npm, $laboratorium, $nama, $email, $prodi, $programming, $mulmed, $networking, $gambar, $alasan, $about, $lolos, $tahun);
					echo "<script>alert('Berhasil mengupload data')</script>";
					return $this->load->view('recruitment.php');
							
			}
			
			
		}
		
        else{
				
				$npm = $this->input->post('npm');
				$nama = $this->input->post('nama');
				$email = $this->input->post('email');
				$prodi = $this->input->post('prodi');
                $laboratorium = $this->input->post('lab');
                $programming = $this->input->post('programming');
				$networking = $this->input->post('networking');
				$mulmed = $this->input->post('mulmed');
				$alasan = $this->input->post('alasan');
				$about = $this->input->post('about');
				$tahun = $this->input->post('tahun');
				$lolos = $this->input->post('status');
				$tanggal=getdate();
                $tahun = $tanggal['year'];
				
					$this->UserRecruitment->edit_surat_kosong_recruitment($npm, $laboratorium, $nama, $email, $prodi, $programming, $mulmed, $networking, $alasan, $about, $lolos, $tahun);
					echo "<script>alert('Berhasil mengupload data')</script>";
					return $this->load->view('recruitment.php');
				
		}
				
	}

	function proses_email(){
		ini_set( 'display_errors', 1 );   
		error_reporting( E_ALL );    
		$from = "rizkiakbar1912@gmail.com";    
		$to = $this->input->post('email');    
		$subject = "Checking PHP mail"; 
		$message = $this->input->post('pesan');   
		$headers = "From:" . $from;
		mail($to,$subject,$message, $headers);    

		echo "Pesan email sudah terkirim.";
	}
	
}
