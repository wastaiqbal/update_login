<?php

class Dataalumni extends CI_Model{

    function hapus_alumni($nim){
        $query=$this->db->query("DELETE FROM  survey_alumni WHERE nim = '$nim'");
        return $query;
    }

    function survey_alumni($nim, $nama,$prodi,$jenis_kelamin,$tlp,
    $stbekerja, $thmasuk, $thlulus, $ipk, $nama_perusahaan, $alamat_perusahaan, 
    $tahun_bekerja, $sallery, $jabatan, $idm1, $idm2, $alasan_belum_bekerja, 
    $profesi, $masukan){

        $query=$this->db->query("INSERT INTO survey_alumni(nim, nama_alumni, prodi, jenis_kelamin, telepon, status_bekerja,
        tahun_masuk, tahun_lulus, ipk, nama_perusahaan, alamat_perusahaan, tahun_bekerja, sallery, jabatan, idm1, idm2,
        alasan_belum_bekerja, profesi, masukan) VALUES 
            ('$nim', '$nama','$prodi','$jenis_kelamin','$tlp',
            '$stbekerja', '$thmasuk', '$thlulus', '$ipk', '$nama_perusahaan', '$alamat_perusahaan', 
            '$tahun_bekerja', '$sallery', '$jabatan', '$idm1', '$idm2', '$alasan_belum_bekerja', 
            '$profesi', '$masukan')");
        return $query;
    }

    function survey_pengguna_alumni($nama,$nama_perusahaan,$alamat_perusahaan,$tahun_bekerja,
    $idm1, $idm2, $profesi, $language, $pti, $komunikasi1, $komunikasi2, $tw, $pd1, $pd2, $pd3, $pd4, $pd5, $pd6,
    $kepuasan, $masukan){
        $query=$this->db->query("INSERT INTO survey_pengguna_alumni(nama, nama_perusahaan, alamat, tahun_bekerja, idm1, idm2, profesi,
         language, pti, komunikasi1, komunikasi2, tw, pd1, pd2, pd3, pd4, pd5, pd6, kepuasan, masukan) VALUES 
         ('$nama','$nama_perusahaan','$alamat_perusahaan','$tahun_bekerja','$idm1', '$idm2', 
         '$profesi', '$language', '$pti', '$komunikasi1', '$komunikasi2', '$tw', '$pd1', '$pd2', '$pd3', '$pd4', '$pd5', '$pd6',
		'$kepuasan', '$masukan')");
        return $query;
    }

    function cek_data_alumni($nim){
        $hasil = $this->db->query("SELECT * FROM survey_alumni WHERE nim = '$nim'");
        return $hasil;
    }

    function edit_data_survey_alumni($nim, $nama,$prodi,$jenis_kelamin,$tlp,
    $stbekerja, $thmasuk, $thlulus, $ipk, $nama_perusahaan, $alamat_perusahaan, 
    $tahun_bekerja, $sallery, $jabatan, $idm1, $idm2, $alasan_belum_bekerja, 
    $profesi, $masukan){
        $hasil = $this->db->query("UPDATE survey_alumni SET 
        nama_alumni='$nama',
        prodi='$prodi', jenis_kelamin='$jenis_kelamin',
        telepon='$tlp', status_bekerja='$stbekerja',
        tahun_masuk='$thmasuk', tahun_lulus='$thlulus',
        ipk='$ipk', nama_perusahaan='$nama_perusahaan',
        alamat_perusahaan='$alamat_perusahaan', tahun_bekerja='$tahun_bekerja',
        sallery='$sallery', jabatan='$jabatan',
        idm1='$idm1', idm2='$idm2',
        alasan_belum_bekerja='$alasan_belum_bekerja',profesi='$profesi',
        masukan='$masukan' WHERE nim='$nim' ");
        return $hasil;
    }

    public function get_edit_data_survey_alumni($nim){       
        $query=$this->db->query("SELECT * FROM survey_alumni WHERE nim='$nim'");
        return $query;
    }

 


}

?>