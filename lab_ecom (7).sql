-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 01 Sep 2020 pada 05.23
-- Versi server: 10.1.34-MariaDB
-- Versi PHP: 7.2.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lab_ecom`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `npm` bigint(15) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `prodi` enum('TI','SI') NOT NULL,
  `Laboratorium` varchar(70) NOT NULL,
  `Jabatan` int(20) NOT NULL,
  `tahun_masuk` int(4) NOT NULL,
  `about` text NOT NULL,
  `foto` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`npm`, `password`, `nama`, `prodi`, `Laboratorium`, `Jabatan`, `tahun_masuk`, `about`, `foto`) VALUES
(0, '$2y$10$B0RQkMkpL6RcR9YUtJTy8ee3JmTArskwBCTV5jLCS.z7sK6Te/GXa', 'winda', 'TI', '1', 5, 0, '', '2e7cffe47e1109dd123945d121351f50.jpg'),
(1, 'L4B1NF0S4TU', 'Asisten Laboratorium', 'TI', '', 0, 2019, '1', '1'),
(17, '$2y$10$K1CndrTrs84/FPK6hIyU7us8IsQd97EuskzZyh.WMRqs9rLF4kDF2', 'MAS DANANG', 'TI', '1', 1, 0, '', '0cbb96a466d10a0fb429705b58c9009d.pdf'),
(112, '$2y$10$hKgfWhwMnQCrENXaV8jeluQVfobVCEuzY0tvYRBF0fvvKCGEZi5Pu', 'syifa', 'TI', '1', 4, 0, '', '49635ab48abf61c3e4c859032f4df9b6.png'),
(120, '$2y$10$WNGGzX0cLY0G.onhUsRLRuWadowwG/QL2/EYj1kFLc8GzjaaXW0ES', 'syifa', 'TI', '1', 4, 0, '', '72f60769496db4be94eba22a3bc8851e.jpg'),
(123, '$2y$10$rlLqv', 'MAS DANANG', 'TI', '1', 1, 0, '', '810afb919e413d653762526fb4656995.jpg'),
(124, '$2y$10$JJxw44t2dnyQXIMWbt1iQOs3pH6qzTIgMME29bZlRrMAfrON/sDM6', 'syifa', 'TI', '1', 4, 0, '', 'c15a3ff3b79645f96087ef378e755f40.jpg'),
(125, '$2y$10$Jrrxu2KlEwGqiB9HxelS6uDxxBNlnJNe0IwXvqat8HlymuYdTOHTe', 'syifa', 'TI', '1', 4, 0, '', '38caeefa54753daf4a5053dcfdfa25e2.jpg'),
(126, '$2y$10$5upupJR/oVqMfJXUlAZ4c.gpKG.NuUULclyAPt2RMJCOYEThoFfky', 'syifa', 'TI', '1', 4, 0, '', '25e9a87d1de82069243a183f997776d2.jpg'),
(127, '$2y$10$IQnCglcuIUBqKyyTehwjzuDxesMzOnjDV40Dina5DqVNbR6Pq7ZWi', 'oin', 'TI', '1', 6, 0, '', '1e27ba54a0368318cc5f054367633158.jpg'),
(190, '$2y$10$c/KXhFr2VrhbWqDK/IiAWe.lSUIlcJAvfSB7C0lSdRQXwM2bEET9m', 'Khairul Refan', 'TI', 'Multimedia and Computer Vision', 2, 0, 'Refan', 'b51d1c8d7ae84b3a04dab426c79db676.jpg'),
(1010, '$2y$10$UmosLBeVe91H2U73U.S4HehPEY/0QSVMq.I5Z4pfkIAcc1Gku0zty', 'Khairul Refan', 'SI', '', 2, 0, 'test', 'fd1787d1a4bf980ebba1b7fe0370e829.jpg'),
(1111, '$2y$10$e0Sp4BBae5sxd..w2Eq6t.AIU7U9RBhrUk/ddW3FMdHAU7CnrS3sm', 'MAS DANANG', 'TI', '1', 1, 0, '', 'f82ada15bf2a8c4a00539f0588775c38.png'),
(1212, '$2y$10$1ep.dLHZBhWGKM/mMOwNiOugF3ms7phUJpzehCLLK8GhPT2ftFRpe', '1212', 'SI', '', 2, 0, '11', '9776ed93322ff1f76f37353f9710583c.jpg'),
(1236, 'qqq', 'syifa', 'TI', '1', 4, 0, '', 'e3d6a97db07bea3b11bcd5b9c4f7c2bf.jpg'),
(11111, '$2y$10$mhyX80VMKxsXQz78CnNN4.x2APNspxe1b1zPobEFf8WYrY9WOtw3a', 'Andrianingsih, S.Kom, MMSI', 'TI', 'E-Commerce', 1, 0, '1', '4c2c2ec7c2e91be2b78091c65c925b0b.jpeg'),
(11112, '$2y$10$4EuFRvkcyatWceRUKDmbqONx.DhMQjDMmGl2v3NOwrH9i/UbQ6EJq', 'Danang Aji Pangestu', 'TI', 'E-Commerce', 2, 0, 'Hallo nama saya danang', '39cd6a2435d599fd3d248ba4042ddda5.jpeg'),
(11113, '$2y$10$.ZI/KGQ3g0dQfpgcWrPN7O92u3NpzoP948n.Nd2lLlHDZ60wHcqG.', 'Muhammad Ridwan', 'SI', 'E-Commerce', 3, 0, '3', 'b7503587fbdd8dd0594e8b818d432fd1.jpeg'),
(11114, '$2y$10$9oMtbHgs/85n0sKWziP0I.0Fr0TX1WDegpUCWVKtLQOII6tPE3v86', 'Syifa Faradilla Fabbrianne', 'SI', 'E-Commerce', 4, 0, 'sipa', 'e9a752d89c0ce8a021a1655247a3095a.jpeg'),
(11115, '$2y$10$cNgT0INMcUxDegynDV9Ca.blnvppbtw8hOnz0nP7d66fEkSosTzOi', 'Winda Antika P', 'SI', 'E-Commerce', 5, 0, 'q', 'b0a8007e2d2c26838aadf970ace8e772.jpeg'),
(11116, '$2y$10$JIGxjEL1KZfA0yNx9u8h5enWDcxI8L20JByIIWq0gVWR8xy6Ai0Du', 'Muhammad Iqbal WP', 'SI', 'E-Commerce', 7, 0, 'I', '8bc6f1b696d363224a996e6acc197e4e.jpeg'),
(11117, '$2y$10$lhXhLnfCMq//RTWkbYGWI.l9tAwIoneot/rmyVt.luMOcpbCC9oY6', 'Rizki Akbar Mahdafiki', 'TI', 'E-Commerce', 7, 0, '3', 'ed701fbabe2f637c83bd3911a5397929.jpeg'),
(11118, '$2y$10$HevEbmgJFmd.mEGt0cq1LuH4e7beryE.VKoQ38TC/0MD6eyManqLO', 'Anggita Putri Maharani', 'SI', 'E-Commerce', 6, 0, '1', '641da6613ee3ef302c04b46e945ba1cb.jpeg'),
(123456, '$2y$10$z.AR0eSfIjmO6lvl6eVw4Ozt4ocqS0LuJwkAXmQ0s9kKcLSBLtqmy', 'winda', 'TI', '1', 5, 0, '', '2f89711206aff926da367df2d6010ba1.jpg'),
(17311289, '$2y$10$z/dZk', 'MAS DANANG', '', '1', 2, 0, '', 'db43fbd03072f9f4d8945c3450681065.jpg'),
(173112706450221, 'lupa', 'Danang Aji Pangestu', 'TI', '', 0, 2019, '', 'me.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `arsip`
--

CREATE TABLE `arsip` (
  `id_dokumen` varchar(20) NOT NULL,
  `nama_softcopy` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `file` varchar(255) NOT NULL,
  `laboratorium` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `arsip`
--

INSERT INTO `arsip` (`id_dokumen`, `nama_softcopy`, `keterangan`, `file`, `laboratorium`) VALUES
('1', 'Danang Aji Pangestu', '1', '0', ''),
('33', '1', '1', '680dc9182391fe8684afb85c14c641a0.pdf', 'E-Commerce');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita_acara`
--

CREATE TABLE `berita_acara` (
  `nama` text NOT NULL,
  `tanggal` text NOT NULL,
  `kode_matkul` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita_acara`
--

INSERT INTO `berita_acara` (`nama`, `tanggal`, `kode_matkul`) VALUES
('QR Code: Agus Iskandar, S.Kom., M.Kom.  1021920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 25 September 2019, 10:39', 1021920),
('QR Code: Novi Dian Nathasia, S.Kom, MMSI  1031920 Sistem Basis Data   2019-2020', 'Wednesday, 25 September 2019, 10:39', 1031920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  1051920 Praktikum Sistem Operasi  2019-2020', 'Wednesday, 25 September 2019, 10:39', 1051920),
('QR Code:  Agus Iskandar, S.Kom., M.Kom.  1071920 Praktikum Sistem Basis Data  2019-2020', 'Wednesday, 25 September 2019, 10:40', 1071920),
('QR Code: Benrahmaf, S.Kom, M.MSI  1081920 Praktikum Pemrograman Multimedia  2019-2020', 'Wednesday, 25 September 2019, 10:40', 1081920),
('QR Code:  Yunan Fauzi Wijaya, S.Kom.,MMSI.  2011920 Grafik Komputer  2019-2020', 'Wednesday, 25 September 2019, 10:41', 2011920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2021920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 25 September 2019, 10:41', 2021920),
('QR Code: Arie Gunawan, S.Kom, MMSI  2031920 Praktikum SIstem Operasi  2019-2020', 'Wednesday, 25 September 2019, 10:42', 2031920),
('QR Code: Novi Dian Nathasia, S.Kom.,MMSi  2051920 Perancangin Basis Data Lanjut  2019-2020', 'Wednesday, 25 September 2019, 10:42', 2051920),
('QR Code: Dr. Fauziah, S Kom. , M.M.S.I.  2061920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 25 September 2019, 10:43', 2061920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2071920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 25 September 2019, 10:44', 2071920),
('QR Code:  Agus Iskandar, S.Kom., M.Kom.  2081920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 25 September 2019, 10:45', 2081920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  4011920 Praktikum Pemrog Multimedia  2019-2020', 'Thursday, 26 September 2019, 08:55', 4011920),
('QR Code:  Rima Tamara Aldisa, S.Kom., M.Kom.  4021920 Praktikum Sistem Operasi  2019-2020', 'Thursday, 26 September 2019, 10:06', 4021920),
('QR Code: Sigit Wijanarko S.T, M.TI  4031920 Data Science  2019-2020', 'Thursday, 26 September 2019, 12:12', 4031920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  4051920 Praktikum Sistem Operasi  2019-2020', 'Thursday, 26 September 2019, 16:40', 1051920),
('QR Code: Nur Hayati, S.Si., M.T.I.  4071920 Analisa Proses Bisnis  2019-2020', 'Friday, 27 September 2019, 08:04', 4071920),
('QR Code: Benrahman, S.Kom, M.MSI  4081920 Deep Learning  2019-2020', 'Friday, 27 September 2019, 08:05', 4081920),
('QR Code: Yunan Fauzi Wijaya, S>Kom.,MMSI.  5011920 Praktikum Pemrog Multimedia  2019-2020', 'Friday, 27 September 2019, 13:29', 5011920),
('QR Code:  Winarsih, S.Si., MMSI  1011920 Praktikum Sistem Basis Data  2019-2020', 'Tuesday, 8 October 2019, 13:30', 1011920),
('QR Code: Agus Iskandar, S.Kom., M.Kom.  1021920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Tuesday, 8 October 2019, 13:31', 1021920),
('QR Code: Novi Dian Nathasia, S&Kom, MMSI  1031920 Sistem Basis Data   2019-2020', 'Tuesday, 8 October 2019, 13:31', 1031920),
('QR Code:  Winarsih, S.Si., MMSI  1041920 Praktikum Sistem Basis Data  2019-2020', 'Tuesday, 8 October 2019, 13:32', 1041920),
('QR Code:  Winarsih, S.Si., MMSI  1061920 Praktikum Pemrograman Visual  2019-2020', 'Tuesday, 8 October 2019, 13:32', 1061920),
('QR Code:  Agus Iskandar, S.Kom., M.Kom.  1071920 Praktikum Sistem Basis Data  2019-2020', 'Tuesday, 8 October 2019, 13:33', 1071920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2021920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Tuesday, 8 October 2019, 13:34', 2021920),
('QR Code: Arie Gunawan, S.Kom, MMSI  2031920 Praktikum SIstem Operasi  2019-2020', 'Tuesday, 8 October 2019, 13:34', 2031920),
('QR Code: Benrahman, S.Kom, M.MSI  2041920 Praktikum Pemrograman Visual  2019-2020', 'Tuesday, 8 October 2019, 15:14', 2041920),
('QR Code: Novi Dian Nathasia, S.Kom.,MMSi  2051920 Perancangan Basis Data Lanjut  2019-2020', 'Tuesday, 8 October 2019, 17:10', 2051920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2061920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Tuesday, 8 October 2019, 19:30', 2061920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2071920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Tuesday, 8 October 2019, 20:59', 2071920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  4011920 Praktikum Pemrog Multimedia  2019-2020', 'Thursday, 10 October 2019, 09:52', 4011920),
('Code93: AK', 'Thursday, 10 October 2019, 10:13', 4021920),
('QR Code: Moh. Iwan Wahyuddin, S.T., M.T.  4041920 Praktikum Sistem Operasi  2019-2020', 'Thursday, 10 October 2019, 15:09', 4041920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  5011920 Praktikum Pemrog Multimedia  2019-2020', 'Friday, 11 October 2019, 10:00', 5011920),
('QR Code:  Winarsih, S.Si., MMSI  1011920 Praktikum Sistem Basis Data  2019-2020', 'Wednesday, 23 October 2019, 09:42', 1011920),
('QR Code: Agus Iskandar, S.Kom., M.Kom.  1021920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 23 October 2019, 09:43', 1021920),
('QR Code: Novi Dian Nathasia, S.Kom, MMSI  1031920 Sistem Basis Data   2019-2020', 'Wednesday, 23 October 2019, 09:43', 1031920),
('QR Code:  Winarsih, S.Si., MMSI  1041920 Praktikum Sistem Basis Data  2019-2020', 'Wednesday, 23 October 2019, 09:44', 1041920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2021920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 23 October 2019, 09:45', 2021920),
('QR Code: Novi Dian Nathasia, S.Kom.,MMSi  2051920 Perancangan Basis Data Lanjut  2019-2020', 'Wednesday, 23 October 2019, 09:46', 2051920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2061920 PRaktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 23 October 2019, 09:46', 2061920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  2071920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 23 October 2019, 09:47', 2071920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  4051920 Praktikum Sistem Operasi  2019-2020', 'Wednesday, 23 October 2019, 10:20', 1051920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  4051920 Praktikum Sistem Operasi  2019-2020', 'Wednesday, 23 October 2019, 10:34', 4051920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  4051920 Praktikum Sistem Operasi  2019-2020', 'Wednesday, 23 October 2019, 10:34', 4051920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  4051920 Praktykum Sistem Operasi  2019-2020', 'Wednesday, 23 October 2019, 10:34', 4051920),
('QR Code: Novi Dian Nathasia, S.Kom.,MMSi  4061920 Perancangan Basis Data Lanjut  2019-2020', 'Wednesday, 23 October 2019, 10:35', 4061920),
('QR Code: Novi Dian Nathasia, S.Kom.,MMSi  4061920 Perancangan Basis Data Lanjut  2019-2020', 'Wednesday, 23 October 2019, 10:35', 4061920),
('QR Code: Novi Dian Nathasia, S.Kom.,MMSi  4061920 Perancangan Basis Data Lanjut  2019-2020', 'Wednesday, 23 October 2019, 10:35', 4061920),
('QR Code: Nur Hayati, S.Si., M.T.I.  4071920 Analisa Proses Bisnis  2019-2020', 'Wednesday, 23 October 2019, 10:36', 4071920),
('QR Code: Nur Hayati, S.Si., M.T.I.  5041920 Praktikum E-Business  2019-2020', 'Wednesday, 23 October 2019, 10:38', 5041920),
('QR Code: Nur Hayati, S.Si., M.T.I.  3031920 Praktikum E-Business  2019-2020', 'Wednesday, 23 October 2019, 15:38', 3031920),
('QR Code: Nur Hayati, S.Si., M.T.I.  3031920 Praktikum E-Business  2019-2020', 'Wednesday, 23 October 2019, 15:38', 3031920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  5051920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Wednesday, 23 October 2019, 15:49', 5051920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  5051920 Praktikum Dasar - Dasar Peirograman I  2019-2020', 'Wednesday, 23 October 2019, 15:50', 5051920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  5051920 Praktikum Dascr - Dasar Pemrograman I  2019-2020', 'Wednesday, 23 October 2019, 15:50', 5051920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  6021920 Praktikum Sistem Operasi  2019-2020', 'Wednesday, 23 October 2019, 15:51', 6021920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  6021920 Praktikum Sistem Operasi  2019-2020', 'Wednesday, 23 October 2019, 15:51', 6021920),
('QR Code: Nur Hayati, S.Si., M.T.I.  6031920 Analisa Proses Bisnis  2019-2020', 'Wednesday, 23 October 2019, 15:52', 6031920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  3061920 Praktikum E-Business  2019-2020', 'Friday, 25 October 2019, 10:16', 3061920),
('QR Code:  Rima Tamara Aldisa, S.Kom., M.Kom.  4021920 Praktikum Sistem Operasi  2019-2020', 'Friday, 25 October 2019, 10:17', 4021920),
('QR Code: Nur Hayati, S.Si., M.T.I.  5041920 Praktikum E-Business  2019-2020', 'Friday, 25 October 2019, 10:22', 5041920),
('QR Code:  Winarsih, S.Si., MMSI  1011920 Praktikum Sistem Basis Data  2019-2020', 'Friday, 25 October 2019, 10:50', 1011920),
('QR Code:  Winarsih, S.Si., MMSI  1061920 Praktikum Pemrograman Visual  2019-2020', 'Friday, 25 October 2019, 10:51', 1061920),
('QR Code:  Winarsih, S.Si., MMSI  1061920 Praktikum Pemrograman Visual  2019-2020', 'Friday, 25 October 2019, 10:51', 1061920),
('QR Code:  Agus Iskandar, S.Kom., M.Kom.  1071920 Praktikum Sistem Basis Data  2019-2020', 'Friday, 25 October 2019, 10:52', 1071920),
('QR Code: Benrahman, S.Kom, M.MSI  1081920 Praktikum Pemrograman Multimedia  2019-2020', 'Friday, 25 October 2019, 10:53', 1081920),
('QR Code: Benrahman, S.Kom, M.MSI  1081920 Praktikum Pemrograman Multimedia  2019-2020', 'Friday, 25 October 2019, 10:53', 1081920),
('QR Code:  Yunan Fauzi Wijaya, S.Kom.,MMSI.  2011920 Grefik Komputer  2019-2020', 'Friday, 25 October 2019, 10:54', 2011920),
('QR Code:  Yunan Fauzi Wijaya, S.Kom.,MMSI.  2011920 Grafik Komputer  2019-2020', 'Friday, 25 October 2019, 10:54', 2011920),
('QR Code: Arie Gunawan, S.Kom, MMSI  2031920 Praktikum SIstem Operasi  2019-2020', 'Friday, 25 October 2019, 10:54', 2031920),
('QR Code: Benrahman, S.Kom, M.MSI  2041920 Praktikum Pemrograman Visual  2019-2020', 'Friday, 25 October 2019, 10:55', 2041920),
('QR Code: Benrahman, S.Kom, M.MSI  2041920 Praktikum Pemrograman Visual  2019-2020', 'Friday, 25 October 2019, 10:56', 2041920),
('QR Code:  Agus Iskandar, S.Kom., M.Kom.  2081920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Friday, 25 October 2019, 10:57', 2081920),
('QR Code:  Agus Iskandar, S.Kom., M.Kom.  2081920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Friday, 25 October 2019, 10:57', 2081920),
('QR Code:  Winarsih, S.Si., MMSI  3011920 Basis Data  2019-2020', 'Friday, 25 October 2019, 10:58', 3011920),
('QR Code:  Winarsih, S.Si., MMSI  3011924 Basis Data  2019-2020', 'Friday, 25 October 2019, 10:58', 3011920),
('QR Code:  Winarsih, S.Si., MMSI  3011920 Basis Data  2019-2020', 'Friday, 25 October 2019, 10:58', 3011920),
('QR Code: Winarsih, S.SI., MMSI  3021920 Basis Data  2019-2020', 'Friday, 25 October 2019, 10:59', 3021920),
('QR Code: Winarsih, S.SI., MMSI  3021920 Basis Data  2019-2020', 'Friday, 25 October 2019, 10:59', 3021920),
('QR Code: Winarsih, S.SI., MMSI  3021920 Basis Data  2019-2020', 'Friday, 25 October 2019, 10:59', 3021920),
('QR Code: Nur Hayati, S.Si., M.T.I.  3031920 Praktikum E-Business  2019-2020', 'Friday, 25 October 2019, 11:00', 3031920),
('QR Code:  Winarsih, S.Si., MMSI  3041920 Sistem Basis Data  2019-2020', 'Friday, 25 October 2019, 11:00', 3041920),
('QR Code:  Winarsih, S.Si., MMSI  3041920 Sistem Basis Data  2019-2020', 'Friday, 25 October 2019, 11:00', 3041920),
('QR Code:  Winarsih,\0S.Si.,DXMSI  3041920 Sistem Basis Data  2019-2020', 'Friday, 25 October 2019, 11:00', 3041920),
('QR Code: Winarsih, S.SI., MMSI  3051920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:01', 3051920),
('QR Code: Winarsih, S.SI., MMSI  3051920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:01', 3051920),
('QR Code: Winarsih, S.SI., MMSI  3051920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:01', 3051920),
('QR Code: Yunan Fauzi Wijaya, S.Knm.,MMSI.  3061920 Praktikum E-Business  2019-2020', 'Friday, 25 October 2019, 11:02', 3061920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  3061920 Praktikum E-Business  2019-2020', 'Friday, 25 October 2019, 11:02', 3061920),
('QR Code: Winarsih, S.SI., MMSI  3071920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:03', 3071920),
('QR Code: Winarsih, S.SI., MMSI  3071920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:03', 3071920),
('QR Code: Winarsih, S.SI., MMSI  3071920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:03', 3071920),
('QR Code: Winarsih, S.SI., MMSI  3081920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:04', 3081920),
('QR Code: Winarsih, S.SI., MMSI  3081920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:04', 3081920),
('QR Code: Winarsih, S.SI., MMSI  3081920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:04', 3081920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  4011920 Praktikum Pemrog Multimedia  2019-2020', 'Friday, 25 October 2019, 11:05', 4011920),
('QR Code: Sigit Wijanarko S.T, M.TI  4031920 Data Science  2019-2020', 'Friday, 25 October 2019, 11:05', 4031920),
('QR Code: Sigit Wijanarko S.T, M.TI  ', 'Friday, 25 October 2019, 11:05', 4031920),
('QR Code: Moh. Iwan Wahyuddin, S.T., M.T.  4041920 Praktikum Sistem Operasi  2019-2020', 'Friday, 25 October 2019, 11:06', 4041920),
('QR Code: Moh. Iwan Wahyuddin, S.T., M.T.  4041920 Praktikum Sistem Operasi  2019-2020', 'Friday, 25 October 2019, 11:06', 4041920),
('QR Code: Nur Hayati, S.Si., M.T.I.  4071920 Analisa Proses Bisnis  2019-2020', 'Friday, 25 October 2019, 11:07', 4071920),
('QR Code: Benrahman, S.Kom, M.MSI  4081920 Deep Learning  2019-2020', 'Friday, 25 October 2019, 11:08', 4081920),
('QR Code: Benrahman, S.Kom, M.MSI  4081920 Deep Learning  2019-2020', 'Friday, 25 October 2019, 11:08', 4081920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  5011920 Praktikum Pemrog Multimedia  2019-2020', 'Friday, 25 October 2019, 11:09', 5011920),
('QR Code: Gatot Supriyono, S.Si., M.S.M.  5021920 Praktikum Sistem Operasi  2019-2020', 'Friday, 25 October 2019, 11:10', 5021920),
('QR Code: Gatot Supriyono, S.Si., M.S.M.  5021920 Praktikum Sistem Operasi  2019-2020', 'Friday, 25 October 2019, 11:10', 5021920),
('QR Code: Gatot Supriyono, S.Si., M.S.M.  5021920 Praktikum Sistem Operasi  2019-2020', 'Friday, 25 October 2019, 11:10', 5021920),
('QR Code: Nur Hayati, S.Si., M.T.I.  5041920 Praktikum E-Business  2019-2020', 'Friday, 25 October 2019, 11:11', 5041920),
('QR Code: Agung Triayudi S.kom., M.Kom  5061920 Rekayasa Perangkat Lunak Sistem Informasi  2019-2020', 'Friday, 25 October 2019, 11:12', 5061920),
('QR Code: Agung Triayudi S.kom., M.Kom  5061920 Rekayasa Perangkat Lunak Sistem Informasi  2019-2020', 'Friday, 25 October 2019, 11:12', 5061920),
('QR Code: Agung Triayudi S.kom., M.Kom  5061920 Rekayasa Perangkat Lunak Sistem Informasi  2019-2020', 'Friday, 25 October 2019, 11:12', 5061920),
('QR Code: Agung Triayudi S.kom., M.Kom  5071920 Basis Daua  2019-2020', 'Friday, 25 October 2019, 11:13', 5071920),
('QR Code: Agung Triayudi S.kom., M.Kom  5071920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:13', 5071920),
('QR Code: Agung Triayudi S.kom., M.Kom  5071920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:13', 5071920),
('QR Code: Agung Triayudi S.kom., M.Kom  5081920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:14', 5081920),
('QR Code: Agung Triayudi S.kom., M.Kom  5081920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:14', 5081920),
('QR Code: Agung Triayudi S.kom., M.Kom  5081920 Basis Data  2019-2020', 'Friday, 25 October 2019, 11:14', 5081920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  6021920 Praktikum Sistem Operasi  2019-2020', 'Friday, 25 October 2019, 11:15', 6021920),
('QR Code: Nur Hayati, S.Si., M.T.I.  6031920 Analisa Proses Bisnis  2019-2020', 'Friday, 25 October 2019, 11:15', 6031920),
('QR Code: Nur Hayati, S.Si., M.T.I.  6031920 Analisa Proses Bisnis  2019-2020', 'Friday, 25 October 2019, 11:15', 6031920),
('QR Code: Winarsih, S.Si., MMSI  6041920 Praktikum Sistem B`sis Data  2019-2020', 'Friday, 25 October 2019, 11:16', 6041920),
('QR Code: Winarsih, S.Si., MMSI  6041920 Praktikum Sistem Basis Data %2019-2020', 'Friday, 25 October 2019, 11:16', 6041920),
('QR Code: Winarshx, S.Si., MMSI  6041920 Praktikum Sistem Basis Data  2019-2020', 'Friday, 25 October 2019, 11:16', 6041920),
('QR Code:  Agus Iskandar, S.Kom., M.Kom.  1071920 Praktikum Sistem Basis Data  2019-2020', 'Monday, 28 October 2019, 19:45', 1081920),
('QR Code:  Winarsih, S.Si., MMSI  1061920 Praktikum Pemrograman Visual  2019-2020', 'Monday, 28 October 2019, 19:46', 1071920),
('QR Code: Yunan Fauzi Wijaya, S.Kom.,MMSI.  4011920 Praktikum Pemrog Multimedia  2019-2020', 'Thursday, 26 December 2019, 11:24', 4011920),
('QR Code: Sigit Wijanario S.T, >jÃ¯Ã­:AÃ™3\04ÂÂ…Ã‘Â„ÂMÂÂ¥Â•Â¹ÂÂ”ÂyÃ -nn\0Â€ÃÃ€\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0', 'Thursday, 26 December 2019, 13:21', 4031920),
('QR Code: Arie Gunawan, S.Kom., MMSI.  4051920 Praktikum Sistem Operasi  2019-2020', 'Thursday, 26 December 2019, 16:41', 4051920),
('QR Code: Nur Hayati, S.Si., M.T.I.  4071920 Analisa Proses Bisnis  2019-2020', 'Thursday, 26 December 2019, 19:20', 4071920),
('QR Code: Gatot Supriyono, S.Si., M.S.M.  5021920 Praktikum Sistem Operasi  2019-2020', 'Friday, 27 December 2019, 11:35', 5021920),
('QR Code: Benrahman, S.Kom, M.MSI  4081920 Deep Learning  2019-2020', 'Friday, 27 December 2019, 11:36', 4081920),
('QR Code: Dr. Fauziah, S. Kom. , M.M.S.I.  5051920 Praktikum Dasar - Dasar Pemrograman I  2019-2020', 'Friday, 27 December 2019, 16:28', 5051920),
('QR Code: Agung Triayudi S.kom., M.Kom  5071920 Basis Data  2019-2020', 'Friday, 27 December 2019, 20:08', 5071920),
('QR Code: Agung Triayudi S.kom., M.Kom  5081920 Basis Dat!  2019-2020', 'Friday, 27 December 2019, 20:08', 5071920);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gaji`
--

CREATE TABLE `gaji` (
  `id_dokumen` varchar(20) NOT NULL,
  `bulan` varchar(20) NOT NULL,
  `tahun` varchar(30) NOT NULL,
  `file` varchar(255) NOT NULL,
  `keterangan` varchar(80) NOT NULL,
  `laboratorium` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `gaji`
--

INSERT INTO `gaji` (`id_dokumen`, `bulan`, `tahun`, `file`, `keterangan`, `laboratorium`) VALUES
('11111', '1', '1', '6dd94c2da9c99caf3b4f74a05d466399.pdf', 'E-Commerce', '1'),
('4', 'Januari', '2020', '045bb363221b737ab5b5e2dffb56d201.pdf', 'Rekap gaji bulan januari', 'E-Commerce'),
('5', 'Februari', '2020', 'ce72f9607867b361132a657754318a12.pdf', 'Rekap gaji bulan februari', 'E-Commerce');

-- --------------------------------------------------------

--
-- Struktur dari tabel `matkul`
--

CREATE TABLE `matkul` (
  `kode_matkul` varchar(20) NOT NULL,
  `Laboratorium` varchar(20) NOT NULL,
  `kelas` varchar(20) NOT NULL,
  `matkul` varchar(80) NOT NULL,
  `hari` enum('SENIN','SELASA','RABU','KAMIS','JUMAT','SABTU') NOT NULL,
  `jam` enum('08.00-09.40','09.50-11.30','11.40-13.20','13.30-15.10','15.20-17.00','17.00-18.40','18.50-20.20','20.20-22.00') NOT NULL,
  `dosen` varchar(90) NOT NULL,
  `qrcode` varchar(255) NOT NULL,
  `tahun_ajaran` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `matkul`
--

INSERT INTO `matkul` (`kode_matkul`, `Laboratorium`, `kelas`, `matkul`, `hari`, `jam`, `dosen`, `qrcode`, `tahun_ajaran`) VALUES
('1011920', '1', '', 'Praktikum Sistem Basis Data ', 'SENIN', '08.00-09.40', ' Winarsih, S.Si., MMSI', '', '2019-2020'),
('1021920', '1', '', 'Praktikum Dasar - Dasar Pemrograman I', 'SENIN', '09.50-11.30', 'Agus Iskandar, S.Kom., M.Kom.', '', '2019-2020'),
('1031920', '1', '', 'Sistem Basis Data ', 'SENIN', '11.40-13.20', 'Novi Dian Nathasia, S.Kom, MMSI', '', '2019-2020'),
('1041920', '1', '', 'Praktikum Sistem Basis Data', 'SENIN', '13.30-15.10', ' Winarsih, S.Si., MMSI', '', '2019-2020'),
('1051920', '1', '', 'Praktikum Sistem Operasi', 'SENIN', '15.20-17.00', 'Arie Gunawan, S.Kom., MMSI.', '', '2019-2020'),
('1061920', '1', '', 'Praktikum Pemrograman Visual', 'SENIN', '17.00-18.40', ' Winarsih, S.Si., MMSI', '', '2019-2020'),
('1071920', '1', '', 'Praktikum Sistem Basis Data', 'SENIN', '18.50-20.20', ' Agus Iskandar, S.Kom., M.Kom.', '', '2019-2020'),
('1081920', '1', '', 'Praktikum Pemrograman Multimedia', 'SENIN', '20.20-22.00', 'Benrahman, S.Kom, M.MSI', '', '2019-2020'),
('2011920', '1', '', 'Grafik Komputer', 'SELASA', '08.00-09.40', ' Yunan Fauzi Wijaya, S.Kom.,MMSI.', '', '2019-2020'),
('2021920', '1', '', 'Praktikum Dasar - Dasar Pemrograman I', 'SELASA', '09.50-11.30', 'Dr. Fauziah, S. Kom. , M.M.S.I.', '', '2019-2020'),
('2031920', '1', '', 'Praktikum SIstem Operasi', 'SELASA', '11.40-13.20', 'Arie Gunawan, S.Kom, MMSI', '', '2019-2020'),
('2041920', '1', '', 'Praktikum Pemrograman Visual', 'SELASA', '13.30-15.10', 'Benrahman, S.Kom, M.MSI', '', '2019-2020'),
('2051920', '1', '', 'Perancangan Basis Data Lanjut', 'SELASA', '15.20-17.00', 'Novi Dian Nathasia, S.Kom.,MMSi', '', '2019-2020'),
('2061920', '1', '', 'Praktikum Dasar - Dasar Pemrograman I', 'SELASA', '17.00-18.40', 'Dr. Fauziah, S. Kom. , M.M.S.I.', '', '2019-2020'),
('2071920', '1', '', 'Praktikum Dasar - Dasar Pemrograman I', 'SELASA', '18.50-20.20', 'Dr. Fauziah, S. Kom. , M.M.S.I.', '', '2019-2020'),
('2081920', '1', '', 'Praktikum Dasar - Dasar Pemrograman I', 'SELASA', '20.20-22.00', ' Agus Iskandar, S.Kom., M.Kom.', '', '2019-2020'),
('3011920', '1', '', 'Basis Data', 'RABU', '08.00-09.40', ' Winarsih, S.Si., MMSI', '', '2019-2020'),
('3021920', '1', '', 'Basis Data', 'RABU', '09.50-11.30', 'Winarsih, S.SI., MMSI', '', '2019-2020'),
('3031920', '1', '', 'Praktikum E-Business', 'RABU', '11.40-13.20', 'Nur Hayati, S.Si., M.T.I.', '', '2019-2020'),
('3041920', '1', '', 'Sistem Basis Data', 'RABU', '13.30-15.10', ' Winarsih, S.Si., MMSI', '', '2019-2020'),
('3051920', '1', '', 'Basis Data', 'RABU', '15.20-17.00', 'Winarsih, S.SI., MMSI', '', '2019-2020'),
('3061920', '1', '', 'Praktikum E-Business', 'RABU', '17.00-18.40', 'Yunan Fauzi Wijaya, S.Kom.,MMSI.', '', '2019-2020'),
('3071920', '1', '', 'Basis Data', 'RABU', '18.50-20.20', 'Winarsih, S.SI., MMSI', '', '2019-2020'),
('3081920', '1', '', 'Basis Data', 'RABU', '20.20-22.00', 'Winarsih, S.SI., MMSI', '', '2019-2020'),
('4021920', '1', '', 'Praktikum Sistem Operasi', 'KAMIS', '09.50-11.30', ' Rima Tamara Aldisa, S.Kom., M.Kom.', '', '2019-2020'),
('4031920', '1', '', 'Data Science', 'KAMIS', '11.40-13.20', 'Sigit Wijanarko S.T, M.TI', '', '2019-2020'),
('4041920', '1', '', 'Praktikum Sistem Operasi', 'KAMIS', '13.30-15.10', 'Moh. Iwan Wahyuddin, S.T., M.T.', '', '2019-2020'),
('4051920', '1', '', 'Praktikum Sistem Operasi', 'KAMIS', '15.20-17.00', 'Arie Gunawan, S.Kom., MMSI.', '', '2019-2020'),
('4061920', '1', '', 'Perancangan Basis Data Lanjut', 'KAMIS', '17.00-18.40', 'Novi Dian Nathasia, S.Kom.,MMSi', '', '2019-2020'),
('4071920', '1', '', 'Analisa Proses Bisnis', 'KAMIS', '18.50-20.20', 'Nur Hayati, S.Si., M.T.I.', '', '2019-2020'),
('4011920', '1', '', 'Praktikum Pemrog Multimedia', 'KAMIS', '08.00-09.40', 'Yunan Fauzi Wijaya, S.Kom.,MMSI.', '', '2019-2020'),
('4081920', '1', '', 'Deep Learning', 'KAMIS', '20.20-22.00', 'Benrahman, S.Kom, M.MSI', '', '2019-2020'),
('5021920', '1', '', 'Praktikum Sistem Operasi', 'JUMAT', '09.50-11.30', 'Gatot Supriyono, S.Si., M.S.M.', '', '2019-2020'),
('5011920', '1', '', 'Praktikum Pemrog Multimedia', 'JUMAT', '08.00-09.40', 'Yunan Fauzi Wijaya, S.Kom.,MMSI.', '', '2019-2020'),
('5031920', '1', '', 'SHALAT JUMAT', 'JUMAT', '11.40-13.20', 'Khotib Masjid', '', '2019-2020'),
('5041920', '1', '', 'Praktikum E-Business', 'JUMAT', '13.30-15.10', 'Nur Hayati, S.Si., M.T.I.', '', '2019-2020'),
('5051920', '1', '', 'Praktikum Dasar - Dasar Pemrograman I', 'JUMAT', '15.20-17.00', 'Dr. Fauziah, S. Kom. , M.M.S.I.', '', '2019-2020'),
('5061920', '1', '', 'Rekayasa Perangkat Lunak Sistem Informasi', 'JUMAT', '17.00-18.40', 'Agung Triayudi S.kom., M.Kom', '', '2019-2020'),
('5071920', '1', '', 'Basis Data', 'JUMAT', '18.50-20.20', 'Agung Triayudi S.kom., M.Kom', '', '2019-2020'),
('5081920', '1', '', 'Basis Data', 'JUMAT', '20.20-22.00', 'Agung Triayudi S.kom., M.Kom', '', '2019-2020'),
('6021920', '1', '', 'Praktikum Sistem Operasi', 'SABTU', '09.50-11.30', 'Arie Gunawan, S.Kom., MMSI.', '', '2019-2020'),
('6031920', '1', '', 'Analisa Proses Bisnis', 'SABTU', '11.40-13.20', 'Nur Hayati, S.Si., M.T.I.', '', '2019-2020'),
('6041920', '1', '', 'Praktikum Sistem Basis Data', 'SABTU', '13.30-15.10', 'Winarsih, S.Si., MMSI', '', '2019-2020'),
('6051920', '1', '', '-', 'SABTU', '15.20-17.00', '-', '', '2019-2020'),
('6061920', '1', '', '-', 'SABTU', '17.00-18.40', '-', '', '2019-2020'),
('6071920', '1', '', '-', 'SABTU', '18.50-20.20', '-', '', '2019-2020'),
('6081920', '1', '', '-', 'SABTU', '20.20-22.00', '-', '', '2019-2020'),
('6011920', '1', '', '-', 'SABTU', '08.00-09.40', '-', '', '2019-2020'),
('0', 'LABORATORIUM E-COMME', '', 'Pemrogrograman Berorirntasi Objek', 'SENIN', '08.00-09.40', '01', '', '2019-2020'),
('0101022021', 'LABORATORIUM E-COMME', 'R03', 'Basis Data Lanjut', 'SENIN', '09.50-11.30', 'Winarsih.', '0101022021-Basis Data Lanjut-SENIN-09.50-11.30-R03-Winarsih..png', '2020-2021'),
('0101022021', 'LABORATORIUM E-COMME', 'R03', 'Basis Data Lanjut', 'SENIN', '09.50-11.30', 'Winarsih.', '0101022021-Basis Data Lanjut-SENIN-09.50-11.30-R03-Winarsih..png', '2020-2021'),
('123', 'E-Commerce', '', 'Danang', '', '', '', '123-Danang----.png', ''),
('12342', 'LABORATORIUM E-COMME', 'R06', 'BASIS DATA LANJUT', 'SENIN', '08.00-09.40', 'Winarsih.', '12342-BASIS DATA LANJUT-SENIN-08.00-09.40-R06-Winarsih..png', '2020-2021'),
('A', 'LABORATORIUM E-COMME', 'R05', 'A', 'SENIN', '08.00-09.40', 'A', 'A-A-SENIN-08.00-09.40-R05-A.png', '2020-2021'),
('123', 'E-Commerce', '', 'Danang', '', '', '', '123-Danang----.png', ''),
('123', 'E-Commerce', '', 'Danang', '', '', '', '123-Danang----.png', ''),
('1', 'LABORATORIUM E-COMME', 'R02', '1', 'SENIN', '11.40-13.20', '1', '1-1-SENIN-11.40-13.20-R02-1.png', '2020-2021'),
('1', 'LABORATORIUM E-COMME', 'R02', '1', 'SENIN', '11.40-13.20', '1', '1-1-SENIN-11.40-13.20-R02-1.png', '2020-2021'),
('123', 'E-Commerce', '', 'Danang', '', '', '', '123-Danang----.png', ''),
('12432', '1', 'R12', '111111', 'SELASA', '09.50-11.30', '11', '12432-111111-SELASA-09.50-11.30-R12-11.png', '2020-2021'),
('10201', 'E-Commerce', 'R03', 'Prak. Pemrograman Berorientasi Objek', 'SENIN', '09.50-11.30', 'Yunan Fauzi Wijaya, S.Kom, MMSI', '10201-Prak. Pemrograman Berorientasi Objek-SENIN-09.50-11.30-R03-Yunan Fauzi Wijaya, S.Kom, MMSI.png', '2020-2021'),
('10301', 'E-Commerce', 'R02', 'Prak. Struktur Data Algoritma', 'SENIN', '11.40-13.20', 'Winarsih, S.Kom, SSI', '10301-Prak. Struktur Data Algoritma-SENIN-11.40-13.20-R02-Winarsih, S.Kom, SSI.png', '2020-2021'),
('10401', 'E-Commerce', 'R04', 'Bahasa dan Automata', 'SENIN', '13.30-15.10', 'Dr. Fauziah, S.Kom, MMSI', '10401-Bahasa dan Automata-SENIN-13.30-15.10-R04-Dr. Fauziah, S.Kom, MMSI.png', '2020-2021'),
('10501', 'E-Commerce', 'R04', 'Pengolahan Citra', 'SENIN', '15.20-17.00', 'Dr. Fauziah, S.Kom, MMSI', '10501-Pengolahan Citra-SENIN-15.20-17.00-R04-Dr. Fauziah, S.Kom, MMSI.png', '2020-2021');

-- --------------------------------------------------------

--
-- Struktur dari tabel `modul`
--

CREATE TABLE `modul` (
  `id_modul` varchar(20) NOT NULL,
  `modul` varchar(20) NOT NULL,
  `matkul` varchar(60) NOT NULL,
  `semester` varchar(8) NOT NULL,
  `upload_by` varchar(90) NOT NULL,
  `laboratorium_upload` varchar(90) NOT NULL,
  `file` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `modul`
--

INSERT INTO `modul` (`id_modul`, `modul`, `matkul`, `semester`, `upload_by`, `laboratorium_upload`, `file`) VALUES
('1232', 'Multimedia', 'Pemrograman Multimedia', 'Genap', 'Khairul Refan', 'Multimedia and Computer Vision', '314176623897e4e5b0c82c9f5867f19c.pdf'),
('1901', 'Java', 'Pemrograman Berorientasi Objek', 'Genap', 'Danang Aji Pangestu', 'E-Commerce', '95943a64f027991b90706d5e75ecc372.docx'),
('1902', 'Visual Basic', 'Pemrograman Visual', 'Ganjil', 'Danang Aji Pangestu', 'E-Commerce', 'e1781a771cbc2d0760ed564afeeb834f.pdf'),
('1903', 'OOP C#', 'Algoritma Pemrograman II', 'Genap', 'Danang Aji Pangestu', 'E-Commerce', '84ef2c9f89e0e7d68536e5abacb8e26d.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengumuman`
--

CREATE TABLE `pengumuman` (
  `pengumuman` text NOT NULL,
  `kriteria` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `perkuliahan`
--

CREATE TABLE `perkuliahan` (
  `matkul` varchar(70) NOT NULL,
  `dosen` varchar(60) NOT NULL,
  `tahun_ajaran` varchar(90) NOT NULL,
  `dokumen` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `requitment`
--

CREATE TABLE `requitment` (
  `npm` bigint(15) NOT NULL,
  `laboratorium` varchar(50) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `prodi` enum('TI','SI') NOT NULL,
  `programing` text NOT NULL,
  `design` text NOT NULL,
  `networking` text NOT NULL,
  `cv` varchar(90) NOT NULL,
  `alasan` text NOT NULL,
  `about` text NOT NULL,
  `lolos` enum('LOLOS','TIDAK') NOT NULL,
  `tahun` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `requitment`
--

INSERT INTO `requitment` (`npm`, `laboratorium`, `nama`, `prodi`, `programing`, `design`, `networking`, `cv`, `alasan`, `about`, `lolos`, `tahun`) VALUES
(183112706450094, 'E-Commerce', 'Muhamad Sopiyan', 'TI', 'untuk saat ini saya bisa menguasai bahasa pemprograman seperti c++ dan c# dengan platformnya dev dan visual studio dan saya tetap akan terus belajar mengenai bahasa pemprogaman lainnya sehingga saya bisa menjadi programmer yang handal nantinya', 'iya saya bisa disign untuk editing photo saya biasa menggunakan software photoshoup dan ilustrator dan untuk editing video saya biasa mengguna adobe primere dan pinecle untuk editing tersebut', 'mengenai networking saya hanya mengetahui beberapa saja karna memang basic saya multimedia yang dasarnya disign, pengetahuan saya mengenai networking seperti menghandle troble shot gangguan pada hub jaringan lemot, tapi saya akan terus  memahami tentang ntworking karna memang pada dasarnya akan bisa karna terbisa dalam memahami ilmu ilmu  tersebut ', 'a2ff4d31772615d486556808dde47d78.pdf', 'karna saya ingin berorganisasi jauh lebih dalam lagi mengenai kegiatan kampus bisa mensharing ilmu - ilmu lainnya dengan teman-teman dan meluangkan waktu banyak dikampus, dan memang pada dasarnya saya mahasiswa reguler yang belum ada kegiatan apapun di luar kanpus saya ingin memanfaatkan waktu yang kosong tersebut  menjadi lebih bermanfaat lagi sehingga pada nantinya bisa digunakan lebih baik lagi', 'nama saya muhamad sopiyan mahasiswa aktif universitas nasional fakultas teknologi komunikasi dan informatikan jurusan informatika angkatan 2018, pendidikan terakhir saya smk wira buana pada jurusan multimedia tahun 2017, alamat saya bojonggede-bogor , usia saya saat ini 21 tahun, pengalaman saya mengenai komputer cukup paham dalam mengoprasikannya dari segi softaware programing, editing maupun office, saya ingin menjadi keluarga bagian dari lab-ecommarce sehingga bisa menambah wawasan dan teman juga nantinya disana ', 'TIDAK', 2020),
(183112706450118, 'E-Commerce', 'Sapto Wibowo', 'TI', 'Memahami bahasa pemrograman c++ dan C#, serta belajar memahami bahasa pemrograman lainnya seperti java.', 'Pernah mengedit foto dengan software adobe photoshop', 'Masih dalam tahap belajar, baru memahami sedikit tentang setting IP', '62e58ec48484816755faf4a323507ca3.pdf', 'Ingin menambah pengalaman dan mendapatkan ilmu lebih selain dari kegiatan belajar di kelas. ', 'Saya bisa berkomunikasi dengan baik, sebisa mungkin bertanggung jawab dalam segala hal, selalu berusaha untuk jujur dan ingin mencoba banyak hal.', 'LOLOS', 2020),
(183112700650112, 'E-Commerce', 'David Ardian Darma', 'SI', 'Bisa, \r\nPHP,HTML,CSS,Boostrap,JAVA,Visual,C#\r\nsaya menguasai php ,html, css, bootstrap, java, visual pada saat masa pembelajaran di sekolah menengah keatas(SMK) karena jurusan saya adalah Software Engineering atau Rekayasa Perangkat Lunak sedangkan C# saya menguasai di masa perkuliahan ini', 'Sedikit menguasai, Software yang digunakan diantaranya adalah Adobe Photoshop, adobe ilustrator,  AVS video editor untuk mengedit video atau Vegas Pro ', 'Sedikit menguasai ,Saya menguasai Networking saat berada di pendidikan sekolah menengah keatas(SMK)\r\ndiajarkan warna warna dari kabel lan dan mengurutkan serta memasang ke dalam komputer lalu saya belajar Cisco Packet Tracer walaupun masih mendasar hanya menyusun skema skema jaringan ', '117e1be1ec8ac5ac9471ce8ce4b7f6b7.pdf', 'Karena Saya ingin mengimplementasikan dan mengembangkan  ilmu yang saya punya lebih baik lagi dan berguna untuk kegiatan lab atau kampus ', 'saya seorang  mahasiswa yang ingin mengembangkan  dan mengimplementasikan ilmu yang sudah diperoleh dari pendidikan sebelumnya  dan membantu orang yang belum mengetahui tentang program sesuai dengan kemampuan yang sudah saya miliki', 'LOLOS', 2020),
(183112700650109, 'E-Commerce', 'Tio Lovian Sinaga', 'SI', 'dasarnya saja yang telah diajarkan didalam perkuliahan c++ dan vis basic dan kalo html saya belajar otodidak dan sekarang saya sedang membuat web sendiri dan itu masih dasarnya saja.', 'bisa jika melalui hp jika di laptop/pc saya belum mahir', 'ntuk prakteknya saya belum bisa tetapi secara teori saya paham', 'efebff7602de04df5aadcb20578ab7f2.pdf', 'karena saya ingin memiliki kegiatan yang lain selain belajar, saya juga ingin mencari pengalam, dan mendapatkan relasi atau ilmu yang ada diluar kelas', 'saya tio umur 20thn, saya gemar berbisnis, bertemu orang baru, menyukai dunia teknologi krn dunia teknologi sangat keren menurut saya alesan itu sy daftar di ftki, sy mempunya skill dlm dunia marketing dan bisnis aplikasi yg saya pakai utk manajemennya adalah ms.word dan excel, sy juga orgnya bertanggung jawab dan berkomitmen kenapa? krn klo tdk ditanamkan sifat itu bisnis sy tidak akan jalan sampai sekarang', 'TIDAK', 2020),
(183112706450071, 'E-Commerce', 'Rizki Akbar Mahdafiki', 'TI', 'Bisa, bahasa C, C++, C# dan VB.Net. Menguasai cara menginput data hingga perulangan dan kondisi', 'Kurang menguasai, Photoshop. Hanya baru mengenal beberapa tools nya', 'Bisa, mulai dari konfigurasi kabel hingga mengatur VLAN pada switch cisco', '5451be5255b15e6d8fe7e9ec1b0e6021.pdf', 'Supaya dapat bermanfaat bagi orang lain dan menambah pengalaman terutama dalam hal menjaga tanggung jawab.', 'Saya adalah seorang yang bisa beradapatasi dengan baik, menyukai dan mau mempelajari hal baru. Saya mudah panik, tapi biasanya saya akan memfokuskan pikiran terlebih dahulu untuk menghilangkan rasa panik tersebut.', 'LOLOS', 2020),
(183112700650181, 'E-Commerce', 'Muhamad Iqbal Wasta Purnama', 'SI', 'Saya bisa dan mengerti beberapa bahasa pemrograman seperti Java, C#, Visual Basic, Html, Css, dan Javascript.\r\nSalah satu bahasa pemrograman yang saya sukai adalah C#, Visual Basic, Html, dan Css. Terimakasih', 'Desain gambar saya mengerti sedikit menggunakan Photoshop,Desain aplikasi saya mengerti menggunakan Visual Basic dan Windows Aplication C#, dan saya juga mengerti tentang beberapa cara mendesain website menggunakan Css/Javascript. Terimakasih', 'Baru sedikit yang saya ketahui mengenai suatu jaringan dan hanya baru berupa teori, namun saya sedang dan akan selalu mempelajari hal mengenai jaringan .Terimakasih', '39eeb403b6dcce5d8e321f31002291af.pdf', 'Saya ingin selalu berkembang dengan mendaftarkan diri sebagai asisten laboratorium ini untuk berlajar dan mencoba hal-hal baru. Terimakasih', 'Nama saya Muhamad Iqbal Wasta Purnama, saat ini saya berada di semester 3 jurusan Sistem Infromasi, Universitas Nasional. Hobi saya yaitu bermain musik, jalan-jalan, Membaca, Bermain, dan mengeksplore hal baru. Saya sangat suka berorganisasi, berkumpul, dan saling berbagi hal yang menarik. Sekian, terimakasih', 'LOLOS', 2020),
(183112706450191, 'E-Commerce', 'Putra Jaya', 'TI', 'Bisa Dasar Bahasa Pemrograman C++ dan HTML', 'Bisa, Untuk mendesain saya memakai Adobe Photoshop sedangkan untuk editing video saya menggunakan Adobe Premier Pro', 'Bisa, Mengerti jaringan berbasis topologi, Mengerti membuat kabel LAN ( Cross & Straight ) & Crimping, Paham dengan perangkat\" jaringan', '56e62852dfe008bf7215ca0fdba5fa0f.pdf', 'Saya ingin menambah pengalaman dengan bekerja sebagai asisten lab, dan ingin menambah ilmu dalam dunia IT dan dengan menjadi asisten lab saya ingin belajar dan mengasah dan memperdalam ilmu & softskill saya menjadi jauh lebih baik lagi.', 'Nama saya Putra Jaya, saya pekerja cepat, pemikir keras, orang yang simple dan garibet, gampang bergaul dengan orang, dan selalu open minded pada semua orang. suka dengan hal yang berhubungan dengan minimalis. bisa bekerja dibawah tekanan. kalo punya satu keinginan ambisi saya dalam mencapai keinginan saya harus tercapai dan berhasil. dan saya gak baperan orangnya.', 'TIDAK', 2020),
(183112700650178, 'E-Commerce', 'Safira Maulidina', 'SI', 'Saya dapat menggunakan bahasa pemrograman dasar Java, C++ dan C# dengan baik', 'Saya dapat menggunakan Adobe Photoshop CC/CS dengan baik.', 'Saya dapat melakukan (troubleshooting) atau analisis terhadap masalah jaringan dasar seperti komputer yang tidak dapat terhubung dengan internet karena masalah wifi ataupun kerusakan pada kabel LAN dengan cukup baik.', '78c48657a1ea42df36d9dc87838ef8de.pdf', 'Saya ingin mencari pengalaman serta melatih diri saya sendiri. Menurut saya, menjadi Asisten merupakan pekerjaan, hanya saja dalam lingkup yang kecil, yaitu lingkup perkuliahan dan itu merupakan suatu kesempatan untuk mencari pengalaman serta melatih diri sebelum nantinya saya terjun ke dalam dunia pekerjaan yang sesungguhnya.', 'Saya Safira Maulidina. Saya seorang mahasiswa aktif semester 3 FTKI Unas jurusan Sistem Informasi. Saya menyadari bahwa saya menyenangi hal-hal yang berbau teknologi, komputer salah satunya, sejak saya SMA. Dengan kuliah di jurusan Sistem Informasi, saya ingin menambah wawasan dan pengetahuan saya tentang teknologi komputer. Saya memiliki minat dan semangat bekerja yang tinggi, mampu mempelajari hal-hal yang baru dengan cepat, disiplin, jujur, dan juga bertanggung jawab.', 'TIDAK', 2020),
(183112700650175, 'E-Commerce', 'Anggita Putri Maharani', 'SI', 'Saya dapat menggunakan bahasa pemrograman dasar Java, C++ dan C# dengan baik.', 'Saya biasa menggunakan Adobe Photoshop CC/CS untuk mendesain dengan sangat baik.', 'Saya dapat melakukan troubleshooting atau analisis terhadap masalah jaringan (seperti komputer yang tidak dapat terhubung dengan internet karena ada kerusakan pada kabel LAN atau Wi-Fi) dengan cukup baik.', '2cfc3694d03313326312ec8897550448.pdf', 'Dengan menjadi asisten lab, saya ingin mendapat pengalaman baru juga melatih diri sebelum terjun ke dunia kerja yang sesungguhnya.', 'Nama saya Anggita Putri Maharani. Saya mahasiswi aktif semester 3 di FTKI UNAS program studi Sistem Informasi. Saya sadar bahwa saya menyukai hal-hal yang berkaitan dengan teknologi sedari kecil. Karena itu, dengan berkuliah di jurusan Sistem Informasi ini, saya ingin memperluas wawasan saya di bidang teknologi. Saya memiliki minat dan semangat bekerja yang tinggi, mampu mempelajari hal-hal baru dengan cepat, disiplin, jujur, dan bertanggung jawab.', 'LOLOS', 2020),
(183112706450094, 'Multimedia and Computer Vision', 'Muhamad Sopiyan', 'TI', 'r', 'r', 'r', 'b1f85d592af042470d65578161f05320.pdf', 'r', 'r', '', 2020);

-- --------------------------------------------------------

--
-- Struktur dari tabel `struktur`
--

CREATE TABLE `struktur` (
  `nama` varchar(80) NOT NULL,
  `tahun_jabatan` int(5) NOT NULL,
  `jabatan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `struktur`
--

INSERT INTO `struktur` (`nama`, `tahun_jabatan`, `jabatan`) VALUES
('ANDRIANINGSIH S.KOM M.KOM', 2019, 'KEPALA LABORATORIUM'),
('SHOFY NAQIAH', 2019, 'KORDINATOR LABORATORIUM'),
('SYIFA FARADILLA FABRIANNE', 2019, 'SEKRETARIS LABORATORIUM'),
('WINDA ANTIKA PUTRI', 2019, 'PENJADWALAN LABORATORIUM'),
('FACHRUL RAZI PRAWIRA', 2019, 'HUMAS I LABORATORIUM'),
('DANANG AJI PANGESTU', 2019, 'HUMAS II LABORATORIUM'),
('ALDY CANTONA', 2019, 'PENGEMBANGAN I LABORATORIUM'),
('MUHAMMAD RIDWAN', 2019, 'PENGEMBANGAN II LABORATORIUM'),
('Andrianingsih, S.Kom, MMSI', 2020, 'KEPALA LABORATORIUM'),
('Danang Aji Pangestu', 2020, 'KORDINATOR LABORATORIUM'),
('Syifa Faradilla Fabrianne', 2020, 'SEKRETARIS LABORATORIUM'),
('Winda Antika Putri', 2020, 'PENJADWALAN LABORATORIUM'),
('Muhammad Ridwan', 2020, 'HUMAS I LABORATORIUM'),
('Muhammad Iqbal', 2020, 'HUMAS II LABORATORIUM'),
('Rizki Akbar Mahdafiki', 2020, 'PENGEMBANGAN I LABORATORIUM'),
('Anggita Putri Maharani', 2020, 'PENGEMBANGAN II LABORATORIUM');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat_keluar`
--

CREATE TABLE `surat_keluar` (
  `nomer` int(10) NOT NULL,
  `perihal` varchar(60) NOT NULL,
  `tujuan` varchar(255) NOT NULL,
  `dokumen` varchar(100) NOT NULL,
  `laboratorium` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `surat_keluar`
--

INSERT INTO `surat_keluar` (`nomer`, `perihal`, `tujuan`, `dokumen`, `laboratorium`) VALUES
(1, '1333', 'DEKAN FTKI', '2d41ea5fccf1d40cde62b3dbd34c712c.pdf', 'E-Commerce');

-- --------------------------------------------------------

--
-- Struktur dari tabel `surat_masuk`
--

CREATE TABLE `surat_masuk` (
  `nomer` varchar(50) NOT NULL,
  `perihal` varchar(70) NOT NULL,
  `dari` varchar(60) NOT NULL,
  `dokumen` varchar(80) NOT NULL,
  `laboratorium` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `surat_masuk`
--

INSERT INTO `surat_masuk` (`nomer`, `perihal`, `dari`, `dokumen`, `laboratorium`) VALUES
('22', '22', '22', '32-99-1-PB.pdf', ''),
('33', 'Pengajuan Barang', 'BAU', '0a449b8237b479d9c6df23fe8c56a815.pdf', ''),
('123', 'Pengajuan Barang Komputer', 'BAU', 'dc9e757140eeded799814ffc4f080ca9.pdf', ''),
('D01', 'Pengajuan Barang a', 'BAU', '1f160d8a6b21bfbfc78725ee3d4479ab.pdf', 'E-Commerce');

-- --------------------------------------------------------

--
-- Struktur dari tabel `waktu_recruitment`
--

CREATE TABLE `waktu_recruitment` (
  `bulan_mulai` int(4) NOT NULL,
  `tanggal` int(4) NOT NULL,
  `jangka` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`npm`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
